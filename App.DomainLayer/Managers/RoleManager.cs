using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using App.Core;
using App.Core.Entities;
using App.Core.Interfaces;
using App.Core.Interfaces.Managers;
using App.Core.Interfaces.Models;
using App.DomainLayer.Models;

namespace App.DomainLayer.Managers
{
  public class RoleManager : IRoleManager
  {
    public IContext Context { get; set; }

    public void AddMemberType(IForumModel forum, string memberType)
    {
      var forumId = forum.Forum.Id;
      var type = Context.Roles.OfType<ForumRole>()
                        .SingleOrDefault(fr => fr.Forum.Id == forumId && fr.Name == memberType);
      if (type == null)
      {
        Context.Roles.Add(new ForumRole { Forum = forum.Forum, Name = memberType });
      }
    }

    public void RemoveMemberType(IForumModel forum, string memberType)
    {
      var forumId = forum.Forum.Id;
      var type = Context.Roles.OfType<ForumRole>()
                        .SingleOrDefault(fr => fr.Forum.Id == forumId && fr.Name == memberType);
      if (type != null)
      {
        Context.Roles.Remove(type);
      }
    }

    public void PromoteMemberToAdmin(IMemberModel member, IForumModel forum)
    {
      var forumAdminRole = Context.GetAdminRole(forum.Forum);
      member.PromoteToAdmin(forumAdminRole);
    }

    public void RevokeAdminRole(IMemberModel member, IForumModel forum)
    {
      var adminRole = Context.GetAdminRole(forum.Forum);
      var roles = member.Member.Roles.OfType<ForumRole>();
      if (roles.Contains(adminRole))
        member.Member.Roles.Remove(adminRole);
    }

    public void PromoteMemberToModerator(IMemberModel member, ISubforumModel subforum)
    {
      var moderatorRole = Context.GetModeratorRole(subforum.Subforum);
      member.PromoteToModerator(moderatorRole);
    }

    public void RevokeModeratorRole(IMemberModel member, ISubforumModel subforum)
    {
      var moderatorRole = Context.GetModeratorRole(subforum.Subforum);
      var roles = member.Member.Roles.OfType<SubforumRole>();
      if (roles.Contains(moderatorRole))
        member.Member.Roles.Remove(moderatorRole);
    }

    public IEnumerable<IMemberModel> GetSuperAdmins()
    {
      return
        Context.Roles.Include(r => r.Members)
               .Single(r => r.Name == Roles.SuperAdmin)
               .Members.Select(m => new MemberModel(m))
               .ToList();
    }

    public IEnumerable<IMemberModel> GetSuperAdminsAsNewMembers(IForumModel forum)
    {
      return
        Context.Roles.Include(r => r.Members)
               .Single(r => r.Name == Roles.SuperAdmin)
               .Members.Select(m => new MemberModel(new Member
               {
                 Name = m.Name,
                 Email = m.Email,
                 Password = m.Password,
                 SecurityQuestion = m.SecurityQuestion,
                 SecurityAnswer = m.SecurityAnswer,
                 MemberSince = m.MemberSince,
                 IsLoggedIn = m.IsLoggedIn,
                 IsActive = m.IsActive,
                 IsConfirmed = m.IsConfirmed,
                 Forum = forum.Forum
               }));
    }

    public IEnumerable<IForumModel> GetAdminForums(int adminId)
    {
      return Context.GetAdminForums(adminId).Select(f => new ForumModel(f));
    }

    public IEnumerable<ISubforumModel> GetModeratorSubforums(int moderatorId)
    {
      return Context.GetModeratorSubforums(moderatorId).Select(s => new SubforumModel(s));
    }
  }
}