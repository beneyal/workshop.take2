﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using App.Core.Entities;
using App.Core.Interfaces;
using App.Core.Interfaces.Managers;
using App.Core.Interfaces.Models;
using App.DomainLayer.Models;
using App.Core;

namespace App.DomainLayer.Managers
{
  public class MemberManager : IMemberManager
  {
    public void AddMember(IMemberModel newMember)
    {
      if (Exists(newMember))
        return;
      var member = newMember.Member;
      Context.Members.Add(member);
    }

    public IMemberModel GetMember(int memberId)
    {
      return new MemberModel(Context.GetMember(memberId));
    }

    public IMemberModel GetMember(string memberName, int forumId)
    {
      return new MemberModel(Context.Members
                                    .Include(m => m.Forum)
                                    .Include(m => m.Friends)
                                    .Include(m => m.Posts)
                                    .Include(m => m.Friends.Select(f => f.Forum))
                                    .Include(m => m.Posts)
                                    .SingleOrDefault(m => m.Name == memberName));
    }


    public IEnumerable<Member> GetAll()
    {
      return Context.Members.Include(m => m.Forum).ToList();
    }

    public void DeleteMember(int memberId)
    {
      var member = Context.Members.Find(memberId);
      Context.Members.Remove(member);
    }

    public void Confirm(string name, string email)
    {
      var member = Context.Members.SingleOrDefault(m => m.Name == name && m.Email == email && !m.IsConfirmed);
      if (member == null)
        return;
      member.IsConfirmed = true;
      member.IsActive = true;
    }

    public void Logout(string token)
    {
      var id = LoginToken.MemberId(token);
      var member = Context.Members.Find(id);
      member.IsLoggedIn = false;
      member.Token = string.Empty;
    }

    public Member AddFriend(int memberId, int friendId)
    {
      var member = Context.GetMember(memberId);
      var friend = Context.GetMember(friendId);
      if (member != null) member.Friends.Add(friend);
      return friend;
    }

    public ICollection<Member> GetFriends(int memberId)
    {
      var singleOrDefault = Context.GetMember(memberId);
      return singleOrDefault != null ? singleOrDefault.Friends : null;
    }

    public void DeleteFriend(int memberId, int friendId)
    {
      var member = Context.GetMember(memberId);
      var memberFriend = Context.GetMember(friendId);
      if (member != null) member.Friends.Remove(memberFriend);
    }

    public void BanMember(int memberId)
    {
      var member = Context.GetMember(memberId);
      member.BanTime = DateTime.Now;
      member.IsActive = false;
    }

    public bool Exists(string email)
    {
      return Context.Members.Any(member => member.Email == email);
    }

    public bool Matches(string requestEmail, int requestedMemberId)
    {
      return Context.Members.Any(m => m.Id == requestedMemberId && m.Email == requestEmail);
    }

    public IContext Context { get; set; }

    private bool Exists(IMemberModel member)
    {
      return Context.Members.Any(f => f.Name == member.Member.Name);
    }
  }
}