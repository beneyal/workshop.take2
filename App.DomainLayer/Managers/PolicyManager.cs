﻿using System.Collections.Generic;
using System.Linq;
using App.Core.Entities;
using App.Core.Interfaces;
using App.Core.Interfaces.Managers;
using App.Core.Interfaces.Models;
using System;
using System.Data.Entity;
using App.Core;
using App.DomainLayer.Models;



namespace App.DomainLayer.Managers
{
  public class PolicyManager : IPolicyManager
  {
    public void AddPolicy(IPolicyModel newPolicy, IForumModel forum)
    {
      forum.Forum.Policy = newPolicy.Policy;
      Context.Policies.Add(newPolicy.Policy);
    }

    public bool CheckedMaximumModeratorAmount(int forumId)
    {
      var policy = Context.Policies.Find(forumId);
      var forum = Context.Forums.Find(forumId);
      var moderatorAmount = forum.Subforums.Aggregate(0, (current, subforum) => current + Context.GetModeratorRole(subforum).Members.Count());
      return policy.MaximumModeratorAmount >= moderatorAmount;
    }

    public bool CheckedMaximumAdministratorAmount(int forumId)
    {
      var policy = Context.Policies.Find(forumId);
      var forum = Context.Forums.Find(forumId);
      var administratorAmount = Context.GetAdminRole(forum).Members.Count();
      return policy.MaximumAdministratorAmount >= administratorAmount;
    }

    public bool CheckedPostsToBecomeModerator(int subforumId, int memberId)
    {
      var subforum = Context.Subforums.Find(subforumId);
      var policy = Context.Policies.Find(subforum.ParentForum);
      var member = Context.Members.Find(memberId);
      var postNum = member.Posts.Count(post => post.Subforum == subforum);
      return policy.PostsToBecomeModerator <= postNum;
    }

    public bool CheckedPostsToBecomeAdministrator(int forumId, int memberId)
    {
      var forum = Context.Forums.Find(forumId);
      var policy = Context.Policies.Find(forum);
      var member = Context.Members.Find(memberId);
      var postNum = forum.Subforums.Aggregate(0, (current, subforum) => current + member.Posts.Count(post => post.Subforum == subforum));
      return policy.PostsToBecomeAdministrator <= postNum;
    }

    public bool CheckedPostsToBecomeGoldMember(int memberId)
    {
      var member = Context.Members.Find(memberId);
      var forum = member.Forum;
      var policy = Context.Policies.Find(forum);
      var postNum = forum.Subforums.Aggregate(0, (current, subforum) => current + member.Posts.Count(post => post.Subforum == subforum));
      return policy.PostsToBecomeGoldMember <= postNum;
    }

    public bool CheckedPostsToBecomeSilverMember(int memberId)
    {
      var member = Context.Members.Find(memberId);
      var forum = member.Forum;
      var policy = Context.Policies.Find(forum);
      var postNum = forum.Subforums.Aggregate(0, (current, subforum) => current + member.Posts.Count(post => post.Subforum == subforum));
      return policy.PostsToBecomeSilverMember <= postNum;
    }

    public bool CheckedDaysToBecomeModerator(int memberId)
    {
      var member = Context.Members.Find(memberId);
      var forum = member.Forum;
      var policy = Context.Policies.Find(forum);
      var daysNum = (member.MemberSince - DateTime.Now).Days;
      return policy.DaysToBecomeModerator <= daysNum;
    }

    public bool CheckedDaysToBecomeAdministrator(int memberId)
    {
      var member = Context.Members.Find(memberId);
      var forum = member.Forum;
      var policy = Context.Policies.Find(forum);
      var daysNum = (member.MemberSince - DateTime.Now).Days;
      return policy.DaysToBecomeAdministrator <= daysNum;
    }

    public void CheckedDaysToBecomeSilverMember()
    {
      foreach (var member in Context.Members)
      {
        BecomeSilverMember(member.Id);
      }
    }

    public void CheckedDaysToBecomeGoldMember()
    {
      foreach (var member in Context.Members)
      {
        BecomeGoldMember(member.Id);
      }
    }

    public void BecomeSilverMember(int memberId)
    {
      var member = Context.Members.Find(memberId);
      var forum = member.Forum;
      var policy = Context.Policies.Find(forum);
      var oldRole = Context.Roles.Find(member.Id == memberId && member.Name == Roles.Regular);
      var daysNum = (oldRole.RoleSince - DateTime.Now).Days;
      if (policy.DaysToBecomeSilverMember == daysNum)
      {
        var newRole = Context.Roles.OfType<ForumRole>().SingleOrDefault(r => r.Name == Roles.Silver && r.Forum == member.Forum);
        newRole.Members.Add(member);
        oldRole.Members.Remove(member);
      }
    }

    public void BecomeGoldMember(int memberId)
    {
      var member = Context.Members.Find(memberId);
      var forum = member.Forum;
      var policy = Context.Policies.Find(forum);
      var oldRole = Context.Roles.Find(member.Id == memberId && member.Name == Roles.Silver);
      var daysNum = (oldRole.RoleSince - DateTime.Now).Days;
      if (policy.DaysToBecomeGoldMember == daysNum)
      {
        var newRole = Context.Roles.OfType<ForumRole>().SingleOrDefault(r => r.Name == Roles.Gold && r.Forum == member.Forum);
        newRole.Members.Add(member);
        oldRole.Members.Remove(member);
      }
    }

    public bool CheckedMinimumPasswordLength(int forumId, string password)
    {
      var forum = Context.Forums.Find(forumId);
      var policy = Context.Policies.Find(forum);
      return policy.MinimumPasswordLength <= password.Length;
    }

    public bool ForbiddenWords(List<String> words, string content)
    {
      foreach (var word in words)
      {
        if (content.Contains(word))
        {
          return false;
        }
      }
      return true;
    }

    public bool SetQuestion(int forumId)
    {
      var forum = Context.Forums.Find(forumId);
      var policy = Context.Policies.Find(forum);
      return policy.SetQuestion;
    }

    public IContext Context { get; set; }
  }
}
