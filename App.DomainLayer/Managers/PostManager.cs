﻿using System.Collections.Generic;
using System.Linq;
using App.Core.Entities;
using App.Core.Interfaces;
using App.Core.Interfaces.Managers;
using App.Core.Interfaces.Models;
using System;
using System.Data.Entity;
using App.Core;
using App.DomainLayer.Models;



namespace App.DomainLayer.Managers
{
  public class PostManager : IPostManager
  {
    public void AddPost(IPostModel newPost, ISubforumModel subforum)
    {
      if (Exists(newPost, subforum)) return;
      subforum.Subforum.Posts.Add(newPost.Post);
      newPost.Post.Subforum = subforum.Subforum;
      Context.Posts.Add(newPost.Post);
    }

    public void AddReplyPost(IPostModel newReplyPost, IPostModel post)
    {
      post.Post.Replies.Add((ReplyPost) newReplyPost.Post);
      Context.Posts.Add((ReplyPost) newReplyPost.Post);
    }

    public void DeletePost(int postId)
    {
      var post = Context.Posts.Find(postId);
      if (post == null)
        return;

      var publisher = Context.Members.Find(post.Publisher.Id);
      publisher.Posts.Remove(post);

      if (post.Replies != null)
      {
        foreach (var reply in post.Replies)
        {
          DeleteReplyPost(reply.Id);
        }
      }
      Context.Posts.Remove(post);
    }

    public void DeleteReplyPost(int id)
    {
      var reply = Context.GetReply(id);
      if (reply == null)
        return;

      var publisher = Context.Members.Include(m => m.Posts).SingleOrDefault(m => m.Id == reply.Publisher.Id);
      if (publisher != null)
        publisher.Posts.Remove(reply);

      var parentPost = reply.ParentPost;
      parentPost.Replies.Remove(reply);

      Context.Posts.Remove(reply);
    }

    public IPostModel GetPost(int id)
    {
      var post = Context.GetPost(id);
      return new PostModel(post);
    }

    public IPostModel GetPostByPublisherId(int publisherId)
    {
      return new PostModel(Context.Posts.SingleOrDefault(p => p.Publisher.Id == publisherId));
    }

    public IEnumerable<Post> GetPostByContent(string content)
    {
      return Context.Posts.Where(p => p.Content.Equals(content)).ToList();
    }

    public IContext Context { get; set; }

    private static bool Exists(IPostModel post, ISubforumModel subforum)
    {
      return subforum.Subforum.Posts.Any(p => p.Title == post.Post.Title);
    }
  }
}
