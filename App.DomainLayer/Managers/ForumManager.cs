using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using App.Core;
using App.Core.Entities;
using App.Core.Interfaces;
using App.Core.Interfaces.Managers;
using App.Core.Interfaces.Models;
using App.DomainLayer.Models;

namespace App.DomainLayer.Managers
{
  public class ForumManager : IForumManager
  {
    public void AddForum(IForumModel newForum)
    {
      if (Exists(newForum)) return;
      var forum = newForum.Forum;
      Context.Roles.Add(new ForumRole { Forum = forum, Name = Roles.Admin });
      Context.Roles.Add(new ForumRole { Forum = forum, Name = Roles.Gold });
      Context.Roles.Add(new ForumRole { Forum = forum, Name = Roles.Silver });
      Context.Roles.Add(new ForumRole { Forum = forum, Name = Roles.Regular });
      Context.Forums.Add(forum);
    }

    public void DeleteForum(int forumId)
    {
      var forum = Context.GetForums().SingleOrDefault(f => f.Id == forumId);
      if (forum == null)
        return;
      var superAdminRole = Context.GetSuperAdminRole();
      foreach (var member in forum.Members.ToList())
      {
        if (member.Roles.Contains(superAdminRole))
        {
          member.Forum = null;
          forum.Members.Remove(member);
        }
        else
          Context.Members.Remove(member);
      }
      Context.Subforums.RemoveRange(forum.Subforums);
      var roles = Context.Roles.OfType<ForumRole>().Include(r => r.Forum).Where(r => r.Forum.Id == forum.Id).ToList();
      Context.Roles.RemoveRange(roles);
      Context.Forums.Remove(forum);
    }

    public void DeleteForum(string forumName)
    {
      var forum = Context.Forums.SingleOrDefault(f => f.Name == forumName);
      if (forum != null)
        DeleteForum(forum.Id);
    }

    public IEnumerable<Forum> GetAll()
    {
      return Context.Forums.Include(f => f.Members).Include(f => f.Subforums).ToList();
    }

    public IForumModel GetForum(int id)
    {
      return
        new ForumModel(Context.Forums.Include(f => f.Members).Include(f => f.Subforums).SingleOrDefault(f => f.Id == id));
    }

    public IForumModel GetForum(string forumName)
    {
      return
        new ForumModel(
          Context.Forums.Include(f => f.Members).Include(f => f.Subforums).SingleOrDefault(f => f.Name == forumName));
    }

    public IContext Context { get; set; }

    private bool Exists(IForumModel forum)
    {
      return Context.Forums.Any(f => f.Name == forum.Forum.Name);
    }
  }
}