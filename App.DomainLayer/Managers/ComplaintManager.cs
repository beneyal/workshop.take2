﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using App.Core.Entities;
using App.Core.Interfaces;
using App.Core.Interfaces.Managers;
using App.Core.Interfaces.Models;
using App.DomainLayer.Models;
using App.Core;

namespace App.DomainLayer.Managers
{
  public class ComplaintManager : IComplaintManager
  {

    public void AddComplaint(IComplaintModel complaintModel)
    {
      var complaint = complaintModel.Complaint;
      Context.Complaints.Add(complaint);
    }

    public IEnumerable<Complaint> GetBySubjectId(int memberId)
    {
      return
        Context.Complaints.Where(c => c.SubjectId == memberId).ToList();
    }

    public IContext Context { get; set; }

  }
}