﻿using System.Net.Mail;
using System.Threading.Tasks;
using App.Core.Interfaces.Managers;
using App.Core.Interfaces.Models;

namespace App.DomainLayer.Managers
{
  public class EmailManager : IEmailManager
  {
    private readonly SmtpClient _client;

    public EmailManager()
    {
      _client = new SmtpClient();
    }

    public void SendConfirmationRequest(IMemberModel member, string confirmationLink)
    {
      var message = new MailMessage
      {
        From = new MailAddress("bgu.forum.generator@gmail.com", "BGU Forum Generator"),
        Subject = "Verify Your Account",
        Body =
          "Please verify your account by visiting the following link: " +
          string.Format("{0}{1}", confirmationLink, member.GenerateConfirmationToken())
      };
      message.To.Add(member.Member.Email);
      _client.Send(message);
    }
  }
}