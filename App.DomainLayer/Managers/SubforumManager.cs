﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using App.Core;
using App.Core.Entities;
using App.Core.Interfaces;
using App.Core.Interfaces.Managers;
using App.Core.Interfaces.Models;
using App.DomainLayer.Models;

namespace App.DomainLayer.Managers
{
  public class SubforumManager : ISubforumManager
  {
    public void AddSubforum(ISubforumModel subforum, IForumModel parent)
    {
      if (Exists(subforum, parent))
        return;
      subforum.SetParent(parent);
      var moderatorRole = new SubforumRole { Subforum = subforum.Subforum, Name = Roles.Moderator };
      Context.Roles.Add(moderatorRole);
      subforum.AddAdministratorsAsModerators(Context.GetAdminRole(parent.Forum), moderatorRole);
      parent.AddSubforum(subforum);
      Context.Subforums.Add(subforum.Subforum);
    }

    public void DeleteSubforum(int subforumId)
    {
      var subforum = Context.GetSubforum(subforumId);
      if (subforum == null)
        return;
      subforum.ParentForum.Subforums.Remove(subforum);
      foreach (var post in subforum.Posts)
      {
        Context.Posts.Remove(post);
      }
      var roles =
        Context.Roles.OfType<SubforumRole>().Include(s => s.Subforum).Where(r => r.Subforum.Id == subforumId).ToList();
      Context.Roles.RemoveRange(roles);
      Context.Subforums.Remove(subforum);
    }

    public void DeleteSubforum(string subject)
    {
      var subforum = Context.Subforums.SingleOrDefault(s => s.Subject == subject);
      if (subforum != null)
        DeleteSubforum(subforum.Id);
    }

    public IEnumerable<Subforum> GetByForumId(int forumId)
    {
      return
        Context.Subforums.Where(s => s.ParentForum.Id == forumId)
               .Include(s => s.ParentForum)
               .Include(s => s.Moderators)
               .Include(s => s.Posts.Select(p => p.Publisher))
               .Include(s => s.Posts.Select(p => p.Replies))
               .ToList();
    }

    public ISubforumModel GetSubforum(int id)
    {
      return
        new SubforumModel(
          Context.Subforums.Include(s => s.ParentForum)
                 .Include(s => s.Moderators)
                 .Include(s => s.Posts.Select(p => p.Publisher))
                 .Include(s => s.Posts.Select(p => p.Replies))
                 .SingleOrDefault(s => s.Id == id));
    }

    public ISubforumModel GetSubforum(string name)
    {
        return
          new SubforumModel(
            Context.Subforums.Include(s => s.ParentForum)
                   .Include(s => s.Moderators)
                   .Include(s => s.Posts.Select(p => p.Publisher))
                   .Include(s => s.Posts.Select(p => p.Replies))
                   .SingleOrDefault(s => s.Subject == name));
    }
    public IContext Context { get; set; }

    private static bool Exists(ISubforumModel subforum, IForumModel parent)
    {
      return parent.Forum.Subforums.Any(s => s.Subject == subforum.Subforum.Subject);
    }
  }
}