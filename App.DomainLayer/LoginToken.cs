﻿using System;
using System.Linq;
using System.Text;
using App.Core;

namespace App.DomainLayer
{
  public static class LoginToken
  {
    private const int InvalidId = -1;
    private const int MemberIdOffset = 0;
    private const int ForumIdOffset = 4;
    private const int TimestampOffset = 8;
    private const int TypeOffset = 32;

    public static string Create(int memberId, int forumId, string userType)
    {
      var id = BitConverter.GetBytes(memberId);
      var forum = BitConverter.GetBytes(forumId);
      var time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
      var key = Guid.NewGuid().ToByteArray();
      var type = Encoding.UTF8.GetBytes(userType);
      return Convert.ToBase64String(id.Concat(forum).Concat(time).Concat(key).Concat(type).ToArray());
    }

    public static string Create(int forumId)
    {
      var id = BitConverter.GetBytes(0);
      var forum = BitConverter.GetBytes(forumId);
      var time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
      var key = Guid.NewGuid().ToByteArray();
      var type = Encoding.UTF8.GetBytes(Roles.Guest);
      return Convert.ToBase64String(id.Concat(forum).Concat(time).Concat(key).Concat(type).ToArray());
    }

    public static string Type(string token)
    {
      try
      {
        var base64String = Convert.FromBase64String(token);
        var length = base64String.Length - TypeOffset;
        var type = new byte[length];
        Array.ConstrainedCopy(base64String, TypeOffset, type, 0, length);
        return Encoding.UTF8.GetString(type);
      }
      catch (FormatException)
      {
        return string.Empty;
      }
    }

    public static int MemberId(string token)
    {
      try
      {
        return BitConverter.ToInt32(Convert.FromBase64String(token), MemberIdOffset);
      }
      catch (FormatException)
      {
        return InvalidId;
      }
    }

    public static int ForumId(string token)
    {
      try
      {
        return BitConverter.ToInt32(Convert.FromBase64String(token), ForumIdOffset);
      }
      catch (FormatException)
      {
        return InvalidId;
      }
    }

    public static DateTime Timestamp(string token)
    {
      try
      {
        return DateTime.FromBinary(BitConverter.ToInt64(Convert.FromBase64String(token), TimestampOffset));
      }
      catch (FormatException)
      {
        return DateTime.MinValue;
      }
    }

    public static bool IsValid(string token)
    {
      return Type(token) != string.Empty &&
             MemberId(token) != InvalidId &&
             ForumId(token) != InvalidId &&
             Timestamp(token) != DateTime.MinValue;
    }
  }
}