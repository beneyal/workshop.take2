﻿using App.Core.Interfaces;
using App.Core.Interfaces.Managers;

namespace App.DomainLayer.Helpers
{
  public class ManagersProvider
  {
    private readonly IContextProvider _provider;

    public ManagersProvider(IContextProvider provider, IForumManager forumManager, ISubforumManager subforumManager,
                            IPostManager postManager, IMemberManager memberManager, IRoleManager roleManager,
                            IEmailManager emailManager, IPolicyManager policyManager, IComplaintManager complaintManager)
    {
      _provider = provider;
      ForumManager = forumManager;
      SubforumManager = subforumManager;
      MemberManager = memberManager;
      PostManager = postManager;
      RoleManager = roleManager;
      EmailManager = emailManager;
      PolicyManager = policyManager;
      ComplaintManager = complaintManager;
    }

    public IForumManager ForumManager { get; private set; }
    public ISubforumManager SubforumManager { get; private set; }
    public IMemberManager MemberManager { get; private set; }
    public IPostManager PostManager { get; private set; }
    public IRoleManager RoleManager { get; private set; }
    public IEmailManager EmailManager { get; private set; }
    public IPolicyManager PolicyManager { get; set; }
    public IComplaintManager ComplaintManager { get; private set; }

    public IContext NewUnitOfWork()
    {
      var uow = _provider.NewContext();
      ForumManager.Context = uow;
      SubforumManager.Context = uow;
      MemberManager.Context = uow;
      PostManager.Context = uow;
      RoleManager.Context = uow;
      PolicyManager.Context = uow;
      ComplaintManager.Context = uow;
      return uow;
    }
  }
}