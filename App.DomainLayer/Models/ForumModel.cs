﻿using System;
using System.Linq;
using App.Core.Entities;
using App.Core.Interfaces.Models;

namespace App.DomainLayer.Models
{
  public class ForumModel : IForumModel
  {
    public ForumModel(Forum forum)
    {
      Forum = forum;
    }

    public Forum Forum { get; private set; }

    public void AddMember(IMemberModel member)
    {
      Forum.Members.Add(member.Member);
    }

    public void DeleteMember(IMemberModel member)
    {
      Forum.Members.Remove(member.Member);
    }

    public IMemberModel GetMember(string username)
    {
      var member = Forum.Members.SingleOrDefault(m => m.Name == username);
      return member == null ? null : new MemberModel(member);
    }

    public void AddSuperAdminAsAdministrator()
    {
      throw new NotImplementedException();
    }

    public void AddSubforum(ISubforumModel subforum)
    {
      Forum.Subforums.Add(subforum.Subforum);
    }
  }
}