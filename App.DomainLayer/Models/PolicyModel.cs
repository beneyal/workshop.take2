﻿using App.Core.Entities;
using App.Core.Interfaces.Models;

namespace App.DomainLayer.Models
{
  public class PolicyModel : IPolicyModel
  {
    public Policy Policy { get; private set; }

    public PolicyModel(Policy policy)
    {
      Policy = policy;
    }

  }
}