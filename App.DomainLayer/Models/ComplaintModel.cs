﻿using App.Core.Entities;
using App.Core.Interfaces.Models;

namespace App.DomainLayer.Models
{
  public class ComplaintModel : IComplaintModel
  {
    public Complaint Complaint { get; private set; }

    public ComplaintModel(Complaint complaint)
    {
      Complaint = complaint;
    }

  }
}