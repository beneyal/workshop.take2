﻿using System;
using System.Linq;
using System.Text;
using App.Core;
using App.Core.Entities;
using App.Core.Interfaces.Models;

namespace App.DomainLayer.Models
{
  public class MemberModel : IMemberModel
  {
    public MemberModel(Member member)
    {
      Member = member;
    }

    public Member Member { get; private set; }

    public void PromoteToAdmin(ForumRole adminRole)
    {
      var roles = Member.Roles.OfType<ForumRole>();
      if (!roles.Contains(adminRole))
        Member.Roles.Add(adminRole);
    }

    public void PromoteToModerator(SubforumRole moderatorRole)
    {
      var roles = Member.Roles.OfType<SubforumRole>();
      if (!roles.Contains(moderatorRole))
        Member.Roles.Add(moderatorRole);
    }

    public string GenerateConfirmationToken()
    {
      var identity = string.Format("{0}:{1}", Member.Name, Member.Email);
      var bytes = Encoding.UTF8.GetBytes(identity);
      return Convert.ToBase64String(bytes);
    }

    public string Login()
    {
      var forumId = Member.Forum != null ? Member.Forum.Id : 0;
      var token = LoginToken.Create(Member.Id, forumId, Roles.Member);
      Member.Token = token;
      Member.IsLoggedIn = true;
      return token;
    }

    public DateTime GetLoginTime()
    {
      return LoginToken.Timestamp(Member.Token);
    }
  }
}