﻿using System.Linq;
using App.Core.Entities;
using App.Core.Interfaces.Models;

namespace App.DomainLayer.Models
{
  public class SubforumModel : ISubforumModel
  {
    public SubforumModel(Subforum subforum)
    {
      Subforum = subforum;
    }

    public Subforum Subforum { get; private set; }

    public void SetParent(IForumModel parent)
    {
      Subforum.ParentForum = parent.Forum;
    }

    public void AddAdministratorsAsModerators(ForumRole adminRole, SubforumRole moderatorRole)
    {
      var admins = Subforum.ParentForum.Members.Where(member => member.Roles.Contains(adminRole));
      foreach (var admin in admins)
      {
        admin.Roles.Add(moderatorRole);
      }
    }
  }
}