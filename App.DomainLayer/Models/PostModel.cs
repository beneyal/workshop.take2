﻿using App.Core.Entities;
using App.Core.Interfaces.Models;

namespace App.DomainLayer.Models
{
  public class PostModel : IPostModel
  {
    public Post Post { get; private set; }

    public PostModel(Post post)
    {
      Post = post;
    }

  }
}