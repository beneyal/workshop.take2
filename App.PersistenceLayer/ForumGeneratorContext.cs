﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using App.Core.Entities;
using App.Core.Interfaces;

namespace App.PersistenceLayer
{
  public class ForumGeneratorContext : DbContext, IContext
  {
    public ForumGeneratorContext()
    {
      Configuration.ProxyCreationEnabled = false;
      Configuration.LazyLoadingEnabled = false;
    }

    public DbSet<Member> Members { get; set; }
    public DbSet<Forum> Forums { get; set; }
    public DbSet<Subforum> Subforums { get; set; }
    public DbSet<Post> Posts { get; set; }
    public DbSet<Role> Roles { get; set; }
    public DbSet<Complaint> Complaints { get; set; }
    public DbSet<Policy> Policies { get; set; }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);
      modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
      modelBuilder.Entity<Forum>().ToTable("Forums");
      modelBuilder.Entity<Forum>().HasMany(f => f.Members).WithOptional(m => m.Forum).WillCascadeOnDelete(true);
      modelBuilder.Entity<Forum>()
                  .HasMany(f => f.Subforums)
                  .WithOptional(sf => sf.ParentForum)
                  .WillCascadeOnDelete(true);
      modelBuilder.Entity<Subforum>().ToTable("Subforums");
      modelBuilder.Entity<ReplyPost>().HasOptional(rp => rp.ParentPost);
      modelBuilder.Entity<Member>().HasMany(m => m.Friends).WithMany();
      modelBuilder.Entity<Subforum>().HasMany(m => m.Moderators).WithMany();
      modelBuilder.Entity<Forum>()
        .HasOptional(f => f.Policy)
        .WithOptionalDependent(p => p.Forum)
        .WillCascadeOnDelete(true);
    }
  }
}