﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using App.Core;
using App.Core.Entities;

namespace App.PersistenceLayer
{
  public class TestingInitializer : DropCreateDatabaseAlways<ForumGeneratorContext>
  {
    protected override void Seed(ForumGeneratorContext context)
    {
      Func<string, Role> newRole = s => new Role { Name = s };
      Func<Forum, string, ForumRole> newForumRole = (f, s) => new ForumRole { Forum = f, Name = s };
      Func<Subforum, string, SubforumRole> newSubforumRole = (sf, s) => new SubforumRole { Subforum = sf, Name = s };
      Func<string, Forum> newForum = s => new Forum { Name = s };
      Func<Forum, string, Subforum> newSubforum = (f, s) =>
                                                  {
                                                    var sf = new Subforum { Subject = s, ParentForum = f };
                                                    f.Subforums.Add(sf);
                                                    return sf;
                                                  };

      Func<string, Member> newMember = s => new Member
      {
        Name = s,
        Email = string.Format("{0}@Wat.com", s),
        Password = "1234".Hash(),
        SecurityQuestion = "Why?",
        SecurityAnswer = "Why not?",
        IsActive = true,
        IsConfirmed = true,
      };

      Func<int, Member, Subforum, Post> newPost = (i, publisher, subforum) =>
      {
        var p = new Post{ Title = string.Format("the title of post {0} in subforum {1}",i,subforum.Id),
                          Content = "the content of the post ...",
                          Publisher = publisher};
        subforum.Posts.Add(p);
        publisher.Posts.Add(p);
        return p;
      };

      Func<int, Member, Post, ReplyPost> newReply = (i, publisher, post) =>
      {
        var rp = new ReplyPost
        {
          Title = string.Format("the title of reply {0} post {1}", i, post.Id),
          Content = "the content of the reply ...",
          Publisher = publisher,
          ParentPost = post
        };
        post.Replies.Add(rp);
        return rp;
      };

      var ben = newMember("Ben");
      var einat = newMember("Einat");
      var nofar = newMember("Nofar");
      var lior = newMember("Lior");
      var slava = newMember("Slava");
      var mira = newMember("Mira");
      var avi = newMember("Avi");
      var achiya = newMember("Achiya");
      var arie = newMember("Arie");
      var azzam = newMember("Azzam");
    

      var forum1 = newForum("Wat");
      var forum2 = newForum("65 Wat");
      var forum3 = newForum("1.21 GigaWat");

      var subforum1 = newSubforum(forum1, "KiloWat1");
      var subforum2 = newSubforum(forum1, "MegaWat1");
      var subforum3 = newSubforum(forum2, "KiloWat2");
      var subforum4 = newSubforum(forum2, "MegaWat2");
      var subforum5 = newSubforum(forum3, "KiloWat3");
      var subforum6 = newSubforum(forum3, "MegaWat3");

      var superAdminRole = newRole(Roles.SuperAdmin);
      var forum1Admin = newForumRole(forum1, Roles.Admin);
      var forum2Admin = newForumRole(forum2, Roles.Admin);
      var forum3Admin = newForumRole(forum3, Roles.Admin);
      var subforum1Mod = newSubforumRole(subforum1, Roles.Moderator);
      var subforum2Mod = newSubforumRole(subforum2, Roles.Moderator);
      var subforum3Mod = newSubforumRole(subforum3, Roles.Moderator);
      var subforum4Mod = newSubforumRole(subforum4, Roles.Moderator);
      var subforum5Mod = newSubforumRole(subforum5, Roles.Moderator);
      var subforum6Mod = newSubforumRole(subforum6, Roles.Moderator);
      var forum1Gold = newForumRole(forum1, Roles.Gold);
      

      ben.Roles.Add(superAdminRole);
      superAdminRole.Members.Add(ben);

      ben.Roles.Add(forum1Admin);
      forum1Admin.Members.Add(ben);

      ben.Roles.Add(forum2Admin);
      forum2Admin.Members.Add(ben);

      ben.Roles.Add(forum3Admin);
      forum3Admin.Members.Add(ben);

      ben.Roles.Add(forum1Gold);
      forum1Gold.Members.Add(ben);

      ben.Roles.Add(subforum1Mod);
      subforum1Mod.Members.Add(ben);

      ben.Roles.Add(subforum2Mod);
      subforum2Mod.Members.Add(ben);
      subforum2.Moderators.Add(ben);

      ben.Roles.Add(subforum3Mod);
      subforum3Mod.Members.Add(ben);
      subforum3.Moderators.Add(ben);

      ben.Roles.Add(subforum4Mod);
      subforum4Mod.Members.Add(ben);
      subforum4.Moderators.Add(ben);

      ben.Roles.Add(subforum5Mod);
      subforum5Mod.Members.Add(ben);
      subforum5.Moderators.Add(ben);

      ben.Roles.Add(subforum6Mod);
      subforum6Mod.Members.Add(ben);
      subforum6.Moderators.Add(ben);

      einat.Roles.Add(forum2Admin);
      forum2Admin.Members.Add(einat);

      nofar.Roles.Add(forum3Admin);
      forum3Admin.Members.Add(nofar);

      nofar.Roles.Add(subforum5Mod);
      subforum5Mod.Members.Add(nofar);
      subforum5.Moderators.Add(nofar);
 
      einat.Forum = forum2;
      nofar.Forum = forum3;
      lior.Forum = forum3;
      slava.Forum = forum1;
      mira.Forum = forum2;
      avi.Forum = forum3;
      achiya.Forum = forum3;
      azzam.Forum = forum2;
      arie.Forum = forum1;

      nofar.Friends.Add(lior);
      lior.Friends.Add(nofar);

      nofar.Friends.Add(avi);
      avi.Friends.Add(nofar);

      forum1.Members.Add(arie);
      forum1.Members.Add(slava);
      
      forum2.Members.Add(einat);
      forum2.Members.Add(mira);
      forum2.Members.Add(azzam);

      forum3.Members.Add(nofar);
      forum3.Members.Add(lior);
      forum3.Members.Add(avi);
      forum3.Members.Add(achiya);
      
      context.Roles.AddRange(new List<Role>
      {
        forum1Gold,
        superAdminRole,
        forum1Admin,
        forum2Admin,
        forum3Admin,
        subforum1Mod,
        subforum2Mod,
        subforum3Mod,
        subforum4Mod,
        subforum5Mod,
        subforum6Mod,
        
      });

      context.Forums.AddRange(new List<Forum> { forum1, forum2, forum3 });
      context.Subforums.AddRange(new List<Subforum> { subforum1, subforum2, subforum3, subforum4, subforum5, subforum6 });
      context.Members.AddRange(new List<Member> { ben, einat, nofar, lior, slava, avi, mira, arie, achiya, azzam });
      context.SaveChanges();

      var post1 = newPost(1,ben, subforum1);
      var post2 = newPost(2,slava,subforum1);
      var post3 = newPost(3,arie,subforum1);
      var post4 = newPost(1,arie,subforum2);
      var post5 = newPost(2,ben,subforum2);
      var post6 = newPost(3,arie,subforum2);
      var post7 = newPost(1,einat,subforum3);
      var post8 = newPost(2,mira,subforum3);
      var post9 = newPost(3,ben,subforum3);
      var post10 = newPost(1,azzam,subforum4);
      var post11 = newPost(2,einat,subforum4);
      var post12 = newPost(3,ben,subforum4);
      var post13 = newPost(1,nofar,subforum5);
      var post14 = newPost(2,lior,subforum5);
      var post15 = newPost(3,avi,subforum5);
      var post16 = newPost(1,achiya,subforum6);
      var post17 = newPost(2,lior,subforum6);
      var post18 = newPost(3,avi,subforum6);

      context.Posts.AddRange(new List<Post>
      {
        post1,
        post2,
        post3,
        post4,
        post5,
        post6,
        post7,
        post8,
        post9,
        post10,
        post11,
        post12,
        post13,
        post14,
        post15,
        post16,
        post17,
        post18
      });
      context.SaveChanges();

      var replypost1 = newReply(1, ben, post2);
      var replypost2 = newReply(2, arie, post2);
      var replypost3 = newReply(1, arie, post4);
      var replypost4 = newReply(1, slava, post5);
      var replypost5 = newReply(1, einat, post7);
      var replypost6 = newReply(1, mira, post9);
      var replypost7 = newReply(2, einat, post9);
      var replypost8 = newReply(1, azzam, post11);
      var replypost9 = newReply(1, nofar, post12);
      var replypost10 = newReply(2, lior, post12);
      var replypost11 = newReply(3, achiya, post12);
      var replypost12 = newReply(1, ben, post15);
      var replypost13 = newReply(2, avi, post15);
      var replypost14 = newReply(1, nofar, post17);
      var replypost15 = newReply(2, lior, post17);

      context.Posts.AddRange(new List<ReplyPost>
      {
        replypost1,
        replypost2,
        replypost3,
        replypost4,
        replypost5,
        replypost6,
        replypost7,
        replypost8,
        replypost9,
        replypost10,
        replypost11,
        replypost12,
        replypost13,
        replypost14,
        replypost15
      });
    }
  }
}