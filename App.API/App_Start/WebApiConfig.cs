﻿using System.Web.Http;
using System.Web.Http.Cors;
using App.API.Filters;

namespace App.API
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
      // Web API configuration and services
      config.EnableCors(new EnableCorsAttribute("*", "*", "*"));

      // Web API routes
      config.MapHttpAttributeRoutes();

      config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}",
                                 new { id = RouteParameter.Optional }
        );

      config.Filters.Add(new MemberAuthorizationAttribute());
    }
  }
}