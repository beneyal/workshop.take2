using System;
using System.Web;
using App.API;
using App.Core.Interfaces;
using App.Core.Interfaces.Managers;
using App.Core.Interfaces.Services;
using App.DomainLayer.Helpers;
using App.DomainLayer.Managers;
using App.PersistenceLayer;
using App.ServiceLayer;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Extensions.Factory;
using Ninject.Web.Common;
using WebActivatorEx;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof (NinjectWebCommon), "Start")]
[assembly: ApplicationShutdownMethod(typeof (NinjectWebCommon), "Stop")]

namespace App.API
{
  public static class NinjectWebCommon
  {
    private static readonly Bootstrapper Bootstrapper = new Bootstrapper();

    /// <summary>
    ///   Starts the application
    /// </summary>
    public static void Start()
    {
      DynamicModuleUtility.RegisterModule(typeof (OnePerRequestHttpModule));
      DynamicModuleUtility.RegisterModule(typeof (NinjectHttpModule));
      Bootstrapper.Initialize(CreateKernel);
    }

    /// <summary>
    ///   Stops the application.
    /// </summary>
    public static void Stop()
    {
      Bootstrapper.ShutDown();
    }

    /// <summary>
    ///   Creates the kernel that will manage your application.
    /// </summary>
    /// <returns>The created kernel.</returns>
    private static IKernel CreateKernel()
    {
      var kernel = new StandardKernel();
      try
      {
        kernel.Bind<Func<IKernel>>()
              .ToMethod(ctx => () => new Bootstrapper().Kernel);
        kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

        RegisterServices(kernel);
        return kernel;
      }
      catch
      {
        kernel.Dispose();
        throw;
      }
    }

    /// <summary>
    ///   Load your modules or register your services here!
    /// </summary>
    /// <param name="kernel">The kernel.</param>
    private static void RegisterServices(IKernel kernel)
    {
      kernel.Bind<ForumGeneratorContext>().ToSelf().InRequestScope();
      kernel.Bind<IContext>().To<ForumGeneratorContext>().InRequestScope();

      kernel.Bind<IContextProvider>().ToFactory();

      kernel.Bind<IForumManager>().To<ForumManager>().InRequestScope();
      kernel.Bind<ISubforumManager>().To<SubforumManager>().InRequestScope();
      kernel.Bind<IMemberManager>().To<MemberManager>().InRequestScope();
      kernel.Bind<IPostManager>().To<PostManager>().InRequestScope();
      kernel.Bind<IRoleManager>().To<RoleManager>().InRequestScope();
      kernel.Bind<IEmailManager>().To<EmailManager>().InRequestScope();
      kernel.Bind<IPolicyManager>().To<PolicyManager>().InRequestScope();
      kernel.Bind<IComplaintManager>().To<ComplaintManager>().InRequestScope();

      kernel.Bind<IForumGeneratorService>()
            .To<ForumGeneratorService>()
            .InRequestScope();
      kernel.Bind<IForumService>().To<ForumService>().InRequestScope();
      kernel.Bind<IMemberService>().To<MemberService>().InRequestScope();
      kernel.Bind<IGuestService>().To<GuestService>().InRequestScope();
      kernel.Bind<IPostService>().To<PostService>().InRequestScope();
      kernel.Bind<ISubforumService>().To<SubforumService>().InRequestScope();
      kernel.Bind<IReportService>().To<ReportService>().InRequestScope();
      kernel.Bind<IPolicyService>().To<PolicyService>().InRequestScope();
      kernel.Bind<IComplaintService>().To<ComplaintService>().InRequestScope();

      kernel.Bind<ServicesBag>().ToSelf().InSingletonScope();
      kernel.Bind<ManagersProvider>().ToSelf().InSingletonScope();
    }
  }
}