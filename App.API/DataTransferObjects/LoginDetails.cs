namespace App.API.DataTransferObjects
{
  public class LoginDetails
  {
    public string Name { get; set; }
    public string Password { get; set; }
  }
}