﻿using System.Collections.Generic;
using System.Linq;
using App.Core.Entities;

namespace App.API.DataTransferObjects
{
  public class ForumDto
  {
    public ForumDto()
    {
    }

    public ForumDto(Forum forum)
    {
      Id = forum.Id;
      Name = forum.Name;
      Members = forum.Members.Select(m => new MemberDto(m)).ToList();
      Subforums = forum.Subforums.Select(s => new SubforumDto(s)).ToList();
    }

    public ForumDto(int id, string name, ICollection<MemberDto> members,
                    ICollection<SubforumDto> subforums)
    {
      Id = id;
      Name = name;
      Members = members;
      Subforums = subforums;
    }

    public int Id { get; set; }
    public string Name { get; set; }
    public ICollection<MemberDto> Members { get; private set; }
    public ICollection<SubforumDto> Subforums { get; private set; }
  }
}