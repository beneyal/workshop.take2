﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Core.Entities;

namespace App.API.DataTransferObjects
{
  public class ReplyPostDto
  {
    public ReplyPostDto()
    {
      Date = DateTime.Now;
      Replies = new List<PostDto>();
    }

    public ReplyPostDto(ReplyPost post)
    {
      Id = post.Id;
      Title = post.Title;
      Content = post.Content;
      Date = post.PublishedOn;
      PublisherId = post.Publisher.Id;
      Replies = post.Replies.Select(r => new PostDto(r)).ToList();
      ParentPost = post.ParentPost.Id;
    }

    public ReplyPostDto(int id, string title, string content, DateTime date, int publisherId,
                   ICollection<PostDto> replies, int parentPost)
    {
      Id = id;
      Title = title;
      Content = content;
      Date = date;
      PublisherId = publisherId;
      Replies = replies;
      ParentPost = parentPost;
    }

    public int Id { get; set; }
    public string Title { get; set; }
    public string Content { get; set; }
    public DateTime Date { get; set; }
    public int PublisherId { get; set; }
    public ICollection<PostDto> Replies { get; set; }
    public int ParentPost { get; set; }
  }
}