﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Core.Entities;

namespace App.API.DataTransferObjects
{
  public class PostDto
  {
    public PostDto()
    {
      Date = DateTime.Now;
      Replies = new List<PostDto>();
    }

    public PostDto(Post post)
    {
      Id = post.Id;
      Title = post.Title;
      Content = post.Content;
      Date = post.PublishedOn;
      PublisherId = post.Publisher.Id;
      Replies = post.Replies.Select(r => new PostDto(r)).ToList();
    }

    public PostDto(int id,string title, string content, DateTime date, int publisherId,
                   ICollection<PostDto> replies)
    {
      Id = id;
      Title = title;
      Content = content;
      Date = date;
      PublisherId = publisherId;
      Replies = replies;
    }

    public int Id { get; set; }
    public string Title { get; set; }
    public string Content { get; set; }
    public DateTime Date { get; set; }
    public int PublisherId { get; set; }
    public ICollection<PostDto> Replies { get; set; }
  }
}