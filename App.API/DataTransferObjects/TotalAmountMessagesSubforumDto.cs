﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Core.Entities;

namespace App.API.DataTransferObjects
{
  public class TotalAmountMessagesSubforumDto
  {
    public TotalAmountMessagesSubforumDto()
    {
    }

//    public ReportDto(Complaint complaint)
//    {
//      Content = complaint.Content;
//      SubjectId = complaint.SubjectId;
//      FiledById = complaint.FiledById;
//      Id = complaint.Id;
//
//    }

    public TotalAmountMessagesSubforumDto(int subforumId, int totalMessages)
    {
      SubforumId = subforumId;
      TotalMessages = totalMessages;

    }

    public int SubforumId { get; set; }
    public int TotalMessages { get; set; }
  }
}