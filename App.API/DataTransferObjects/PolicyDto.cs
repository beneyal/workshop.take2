﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Core.Entities;

namespace App.API.DataTransferObjects
{
  public class PolicyDto
  {

    public PolicyDto()
    {
      Id = 0;
      ForumId = 0;
      MaximumAdministratorAmount = 0;
      MaximumModeratorAmount = 0;
      DaysToBecomeModerator = 0;
      PostsToBecomeModerator = 0;
      DaysToBecomeAdministrator = 0;
      PostsToBecomeAdministrator = 0;
      DaysToBecomeGoldMember = 10;
      PostsToBecomeGoldMember = 0;
      DaysToBecomeSilverMember = 10;
      PostsToBecomeSilverMember = 0;
      DeletePostOnlyByAdministrator = false;
      DeletePostOnlyByPublisher = false;
      SessionLength = 0;
      SessionIdleTime = 0;
      InformOptions = Policy.InformOption.Offline;
      MinimumPasswordLength = 10;
      ForbiddenWords = new List<String>();
      DaysToResetPassword = 0;
      SetQuestion = false;
    }

    public PolicyDto(Policy policy)
    {
      Id = policy.Id;
      ForumId = policy.Forum.Id;
      MaximumAdministratorAmount = policy.MaximumAdministratorAmount;
      MaximumModeratorAmount = policy.MaximumModeratorAmount;
      DaysToBecomeModerator = policy.DaysToBecomeModerator;
      PostsToBecomeModerator = policy.PostsToBecomeModerator;
      DaysToBecomeAdministrator = policy.DaysToBecomeAdministrator;
      PostsToBecomeAdministrator = policy.PostsToBecomeAdministrator;
      DaysToBecomeGoldMember = policy.DaysToBecomeGoldMember;
      PostsToBecomeGoldMember = policy.PostsToBecomeGoldMember;
      DaysToBecomeSilverMember = policy.DaysToBecomeSilverMember;
      PostsToBecomeSilverMember = policy.PostsToBecomeSilverMember;
      DeletePostOnlyByAdministrator = policy.DeletePostOnlyByAdministrator;
      DeletePostOnlyByPublisher = policy.DeletePostOnlyByPublisher;
      SessionLength = policy.SessionLength;
      SessionIdleTime = policy.SessionIdleTime;
      InformOptions = policy.InformOptions;
      MinimumPasswordLength = policy.MinimumPasswordLength;
      ForbiddenWords = policy.ForbiddenWords;
      DaysToResetPassword = policy.DaysToResetPassword;
      SetQuestion = policy.SetQuestion;
    }

    public PolicyDto(int id, int forumId, int maximumAdministratorAmount, int maximumModeratorAmount, int daysToBecomeModerator,
      int postsToBecomeModerator, int daysToBecomeAdministrator, int postsToBecomeAdministrator, int daysToBecomeGoldMember,
      int postsToBecomeGoldMember, int daysToBecomeSilverMember, int postsToBecomeSilverMember, bool deletePostOnlyByAdministrator,
      bool deletePostOnlyByPublisher, int sessionLength, int sessionIdleTime, Policy.InformOption informOptions,
      int minimumPasswordLength, List<String> forbiddenWords, int daysToResetPassword, bool setQuestion)
    {
      Id = id;
      ForumId = forumId;
      MaximumAdministratorAmount = maximumAdministratorAmount;
      MaximumModeratorAmount = maximumModeratorAmount;
      DaysToBecomeModerator = daysToBecomeModerator;
      PostsToBecomeModerator = postsToBecomeModerator;
      DaysToBecomeAdministrator = daysToBecomeAdministrator;
      PostsToBecomeAdministrator = postsToBecomeAdministrator;
      DaysToBecomeGoldMember = daysToBecomeGoldMember;
      PostsToBecomeGoldMember = postsToBecomeGoldMember;
      DaysToBecomeSilverMember = daysToBecomeSilverMember;
      PostsToBecomeSilverMember = postsToBecomeSilverMember;
      DeletePostOnlyByAdministrator = deletePostOnlyByAdministrator;
      DeletePostOnlyByPublisher = deletePostOnlyByPublisher;
      SessionLength = sessionLength;
      SessionIdleTime = sessionIdleTime;
      InformOptions = informOptions;
      MinimumPasswordLength = minimumPasswordLength;
      ForbiddenWords = forbiddenWords;
      DaysToResetPassword = daysToResetPassword;
      SetQuestion = setQuestion;
    }

    public int Id { get; set; }
    public int ForumId { get; set; }
    // forum options
    public int MaximumAdministratorAmount { get; set; }
    public int MaximumModeratorAmount { get; set; }
    public int DaysToBecomeModerator { get; set; }
    public int PostsToBecomeModerator { get; set; }
    public int DaysToBecomeAdministrator { get; set; }
    public int PostsToBecomeAdministrator { get; set; }
    public int DaysToBecomeGoldMember { get; set; }
    public int PostsToBecomeGoldMember { get; set; }
    public int DaysToBecomeSilverMember { get; set; }
    public int PostsToBecomeSilverMember { get; set; }
    public bool DeletePostOnlyByAdministrator { get; set; }
    public bool DeletePostOnlyByPublisher { get; set; }
    public int SessionLength { get; set; }
    public int SessionIdleTime { get; set; }
    public Policy.InformOption InformOptions { get; set; }

    // security options
    public int MinimumPasswordLength { get; set; }
    public List<String> ForbiddenWords { get; set; }
    public int DaysToResetPassword { get; set; }
    public bool SetQuestion { get; set; }
  }
}