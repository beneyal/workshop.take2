using System.ComponentModel.DataAnnotations;

namespace App.API.DataTransferObjects
{
  public class RegistrationDetails
  {
    public string Name { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }

    [Compare("Password")]
    public string ConfirmPassword { get; set; }

    public string Question { get; set; }
    public string Answer { get; set; }
  }
}