using System;
using System.Collections.Generic;
using System.Linq;
using App.Core.Entities;

namespace App.API.DataTransferObjects
{
  public class MemberDto
  {
    public MemberDto()
    {
    }

    public MemberDto(Member member)
    {
      Name = member.Name;
      Password = member.Password;
      Email = member.Email;
      SecurityQuestion = member.SecurityQuestion;
      SecurityAnswer = member.SecurityAnswer;
      ForumId = member.Forum.Id;
      Posts = member.Posts.Select(p => new PostDto(p)).ToList();
      Friends = member.Friends.Select(f => new MemberDto(f)).ToList();
      MemberId = member.Id;
      MemberBanTime = member.BanTime;
    }

    public MemberDto(string name, string password, string email, string securityQuestion, string securityAnswer,
                     int forumId, int memberId)
    {
      Name = name;
      Password = password;
      Email = email;
      SecurityQuestion = securityQuestion;
      SecurityAnswer = securityAnswer;
      ForumId = forumId;
      MemberId = memberId;
    }

    public string Name { get; set; }
    public int MemberId { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
    public string SecurityQuestion { get; set; }
    public string SecurityAnswer { get; set; }
    public int ForumId { get; set; }
    public DateTime MemberBanTime { get; set; }

    public ICollection<PostDto> Posts { get; private set; }
    public ICollection<MemberDto> Friends { get; private set; }
  }
}