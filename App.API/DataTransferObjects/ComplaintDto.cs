﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Core.Entities;

namespace App.API.DataTransferObjects
{
  public class ComplaintDto
  {
    public ComplaintDto()
    {
    }

    public ComplaintDto(Complaint complaint)
    {
      Content = complaint.Content;
      SubjectId = complaint.SubjectId;
      FiledById = complaint.FiledById;
      Id = complaint.Id;

    }

    public ComplaintDto(string content, int subjectId, int filedById, int id)
    {
      Id = id;
      Content = content;
      SubjectId = subjectId;
      FiledById = filedById;

    }

    public string Content { get; set; }
    public int SubjectId { get; set; }
    public int FiledById { get; set; }
    public int Id { get; set; }
  }
}