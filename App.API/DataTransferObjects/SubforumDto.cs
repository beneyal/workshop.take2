﻿using System.Collections.Generic;
using System.Linq;
using App.Core.Entities;

namespace App.API.DataTransferObjects
{
  public class SubforumDto
  {
    public SubforumDto()
    {
    }

    public SubforumDto(Subforum subforum)
    {
      Id = subforum.Id;
      Subject = subforum.Subject;
      Posts = subforum.Posts.Where(p => !(p is ReplyPost)).Select(p => new PostDto(p)).ToList();
    }

    public SubforumDto(int id, string subject, ICollection<PostDto> posts)
    {
      Id = id;
      Subject = subject;
      Posts = posts;
    }

    public int Id { get; set; }
    public string Subject { get; set; }
    public ICollection<PostDto> Posts { get; private set; }
  }
}