﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Core.Entities;
using App.ServiceLayer;

namespace App.API.DataTransferObjects
{
  public class ModeratorByForumsDTO
  {
    public ModeratorByForumsDTO()
    {
    }

//    public ReportDto(Complaint complaint)
//    {
//      Content = complaint.Content;
//      SubjectId = complaint.SubjectId;
//      FiledById = complaint.FiledById;
//      Id = complaint.Id;
//
//    }

//    public ModeratorByForumsDTO(int subforumId, IEnumerable<Member> subforumModerators)
//    {
//      SubforumId = subforumId;
//      AppointMembers = subforumModerators.Select(m => new MemberDto(m)).ToList();
//      var posts = subforumModerators.Select(m => m.Posts).ToList();
//      Post= posts.Select(p => new PostDto(p));
//    }

    public int SubforumId { get; set; }
    public IEnumerable<MemberDto> AppointMembers { get; set; }
    public ICollection<PostDto> Post { get; set; }
  }
}