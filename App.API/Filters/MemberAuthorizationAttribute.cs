﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using App.Core;
using App.DomainLayer;
using App.PersistenceLayer;

namespace App.API.Filters
{
  public class MemberAuthorizationAttribute : AuthorizationFilterAttribute
  {
    public override void OnAuthorization(HttpActionContext actionContext)
    {
      var request = actionContext.Request;
      var authHeader = request.Headers.Authorization;

      if (SkipAuthorization(actionContext))
        return;

      if (authHeader == null ||
          !authHeader.Scheme.Equals("Bearer", StringComparison.OrdinalIgnoreCase) ||
          string.IsNullOrWhiteSpace(authHeader.Parameter) ||
          !LoginToken.IsValid(authHeader.Parameter))
      {
        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
        return;
      }

      if (Thread.CurrentPrincipal.Identity.IsAuthenticated)
        return;

      var token = authHeader.Parameter;
      var forumId = LoginToken.ForumId(token);
      var type = LoginToken.Type(token);
      if (type != Roles.Guest)
      {
        using (var db = new ForumGeneratorContext())
        {
          var id = LoginToken.MemberId(token);
          var member = db.Members.Include(m => m.Forum).SingleOrDefault(m => m.Id == id);
          if (member != null)
          {
            if (member.IsLoggedIn && member.IsActive)
            {
              if (!Expired(token))
              {
                Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(member.Email),
                                                               db.GetRolesArray(member.Id));
                return;
              }
              member.IsLoggedIn = false;
              member.Token = LoginToken.Create(forumId);
            }
          }
        }
      }
      HandleUnauthorized(actionContext, forumId);
    }

    private static bool Expired(string token)
    {
      return LoginToken.Timestamp(token) <= DateTime.UtcNow.AddMinutes(-5);
    }

    private static void HandleUnauthorized(HttpActionContext actionContext, int forumId)
    {
      actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
      var url = ConfigurationManager.AppSettings["url"];
      var authenticationHeaderValue = new AuthenticationHeaderValue("Bearer",
                                                                    string.Format("location='{0}/forum/{1}/login", url,
                                                                                  forumId));
      actionContext.Response.Headers.WwwAuthenticate.Add(authenticationHeaderValue);
    }

    private static bool SkipAuthorization(HttpActionContext actionContext)
    {
      Contract.Assert(actionContext != null);

      return actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any() ||
             actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
    }
  }
}