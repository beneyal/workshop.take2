﻿using System;
using System.Data.Entity;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Http;
using App.PersistenceLayer;
using Newtonsoft.Json;

namespace App.API
{
  public class WebApiApplication : HttpApplication
  {
    protected void Application_Start()
    {
//      var configuration = WebConfigurationManager.OpenWebConfiguration("~");
//      var section = (AppSettingsSection)configuration.GetSection("appSettings");
//      section.Settings["url"].Value = string.Format("http://{0}:20808/api", GetIpAddress());
//      configuration.Save();
//      ConfigurationManager.RefreshSection("appSettings");
      GlobalConfiguration.Configure(WebApiConfig.Register);
      GlobalConfiguration.Configuration.Formatters.JsonFormatter
                         .SerializerSettings.ReferenceLoopHandling =
        ReferenceLoopHandling.Ignore;
      Database.SetInitializer(new TestingInitializer());
      using (var context = new ForumGeneratorContext())
      {
        context.Database.Initialize(true);
      }
    }

    private static string GetIpAddress()
    {
      var address = "";
      var request = WebRequest.Create("http://checkip.dyndns.org/");
      using (var response = request.GetResponse())
      using (var stream = new StreamReader(response.GetResponseStream()))
      {
        address = stream.ReadToEnd();
      }

      var first = address.IndexOf("Address: ", StringComparison.Ordinal) + 9;
      var last = address.LastIndexOf("</body>", StringComparison.Ordinal);
      address = address.Substring(first, last - first);

      return address;
    }
  }
}