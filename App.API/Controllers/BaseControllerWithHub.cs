﻿using System;
using System.Security.Principal;
using System.Threading;
using System.Web.Http;
using App.API.Hubs;
using App.Core;
using App.ServiceLayer;
using Microsoft.AspNet.SignalR;

namespace App.API.Controllers
{
  public abstract class BaseControllerWithHub : ApiController
  {
    private readonly Lazy<IHubContext> _hub =
      new Lazy<IHubContext>(() => GlobalHost.ConnectionManager.GetHubContext<ForumNotificationHub>());

    protected IHubContext Hub
    {
      get { return _hub.Value; }
    }

    protected BaseControllerWithHub(ServicesBag services)
    {
      Services = services;
    }

    protected bool IsSuperAdmin()
    {
      return Thread.CurrentPrincipal.IsInRole(Roles.SuperAdmin);
    }

    protected bool IsAdmin(int forumId)
    {
      return Thread.CurrentPrincipal.IsInRole(string.Format("{0}:{1}", Roles.Admin, forumId));
    }

    protected bool IsModerator(int subforumId)
    {
      return Thread.CurrentPrincipal.IsInRole(string.Format("{0}:{1}", Roles.Moderator, subforumId));
    }

    protected bool IsGold(int forumId)
    {
      return Thread.CurrentPrincipal.IsInRole(string.Format("{0}:{1}", Roles.Gold, forumId));
    }

    protected bool IsSilver(int forumId)
    {
      return Thread.CurrentPrincipal.IsInRole(string.Format("{0}:{1}", Roles.Silver, forumId));
    }

    protected bool IsRegular(int forumId)
    {
      return Thread.CurrentPrincipal.IsInRole(string.Format("{0}:{1}", Roles.Regular, forumId));
    }

    public ServicesBag Services { get; private set; }
  }
}
