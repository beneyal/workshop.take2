﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web.Http;
using App.API.DataTransferObjects;
using App.Core.Entities;
using App.ServiceLayer;
using Ninject.Activation;

namespace App.API.Controllers
{
  public class SubforumsController : BaseControllerWithHub
  {
    public SubforumsController(ServicesBag services) : base(services)
    {
    }

    [Route("api/forums/{forumId}/subforums")]
    public IHttpActionResult Post([FromUri] int forumId, [FromBody] string name)
    {
      if (!IsSuperAdmin() && !IsAdmin(forumId))
        return StatusCode(HttpStatusCode.Unauthorized);
      return
        Ok(string.Format("{0}/subforums/{1}",
                         ConfigurationManager.AppSettings["url"],
                         Services.SubforumService.CreateSubforum(forumId, name).Id));
    }

    public IHttpActionResult Delete(int id)
    {
      var forumId = Services.SubforumService.GetSubforum(id).ParentForum.Id;
      if (!IsSuperAdmin() && !IsAdmin(forumId))
        return StatusCode(HttpStatusCode.Unauthorized);
      Services.SubforumService.DeleteSubforum(id);
      return StatusCode(HttpStatusCode.NoContent);
    }

    [AllowAnonymous]
    public IHttpActionResult Get(int id)
    {
      var subforum = Services.SubforumService.GetSubforum(id);
      if (subforum != null)
      {
        return Ok(new SubforumDto(subforum));
      }
      return BadRequest("The requested subforum does not exist.");
    }

    [AllowAnonymous]
    public IHttpActionResult Get(string name)
    {
        var subforum = Services.SubforumService.GetSubforum(name);
        if (subforum != null)
        {
            return Ok(new SubforumDto(subforum));
        }
        return BadRequest("The requested subforum does not exist.");
    }

    [AllowAnonymous]
    [Route("api/forums/{forumId}/subforums")]
    public IHttpActionResult GetSubforums(int forumId)
    {
      return Ok(Services.SubforumService.GetSubforums(forumId).Select(s => new SubforumDto(s)).ToList());
    }

    [AllowAnonymous]
    [HttpGet]
    [Route("api/subforums/moderators/{moderatorId}")]
    public IHttpActionResult GetModeratorSubforum(int moderatorId)
    {
      var subforums = Services.SubforumService.GetModeratorSubforums(moderatorId) as IList<Subforum> ??
                      Services.SubforumService.GetModeratorSubforums(moderatorId).ToList();
      if (subforums.Any())
        return Ok(subforums.Select(s => new SubforumDto(s)).ToList());
      return Ok(string.Format("member {0} doesn't moderate subforums", moderatorId));
    }

    [Route("api/subforums/{subforumId}/moderators")]
    public IHttpActionResult Post(int subforumId, [FromBody] int memberId)
    {
      var forumId = Services.SubforumService.GetSubforum(subforumId).ParentForum.Id;
      if (!IsSuperAdmin() && !IsAdmin(forumId))
        return StatusCode(HttpStatusCode.Unauthorized);
      Services.SubforumService.PromoteToModerator(memberId, subforumId);
      return
        Ok(string.Format("member {0} added as moderator to subforum {1}",
          memberId, subforumId));
    }

    [Route("api/subforums/{subforumId}/moderators/{memberId}")]
    public IHttpActionResult Delete([FromUri] int subforumId, [FromUri] int memberId)
    {
      var forumId = Services.SubforumService.GetSubforum(subforumId).ParentForum.Id;
      if (!IsSuperAdmin() && !IsAdmin(forumId))
        return StatusCode(HttpStatusCode.Unauthorized);
      Services.SubforumService.DemoteModerator(memberId, subforumId);
      return Ok(string.Format("member {0} removed from subforum {1} moderators", memberId, subforumId));
    }

    [HttpGet]
    [Route("api/subforums/{subforumId}/total-posts")]
    public IHttpActionResult GetNumberOfMessagesForSubforum(int subforumId)
    {
      var subforum = Services.SubforumService.GetSubforum(subforumId);
      if (subforum != null)
        return Ok(new TotalAmountMessagesSubforumDto(subforumId,subforum.Posts.Count));
      return Ok(string.Format("Subforum {0} doesn't exists.", subforumId));
    }
  }
}
