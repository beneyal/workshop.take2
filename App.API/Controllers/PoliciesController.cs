﻿using System.Configuration;
using System.Web.Http;
using App.API.DataTransferObjects;
using App.ServiceLayer;

namespace App.API.Controllers
{

  public class PoliciesController : BaseControllerWithHub
  {
    public PoliciesController(ServicesBag services) : base(services)
    {
    }

    [Route("api/forums/{forumId}/policies")]
    public IHttpActionResult Post([FromUri] int forumId, [FromBody] PolicyDto policyDto)
    {
      var policy = Services.PolicyService.SetNewPolicy(forumId, policyDto.Id, policyDto.MaximumAdministratorAmount,
                                                       policyDto.MaximumModeratorAmount, policyDto.DaysToBecomeModerator,
                                                       policyDto.PostsToBecomeModerator,
                                                       policyDto.DaysToBecomeAdministrator,
                                                       policyDto.PostsToBecomeAdministrator,
                                                       policyDto.DaysToBecomeGoldMember,
                                                       policyDto.PostsToBecomeGoldMember,
                                                       policyDto.DaysToBecomeSilverMember,
                                                       policyDto.PostsToBecomeSilverMember,
                                                       policyDto.DeletePostOnlyByAdministrator,
                                                       policyDto.DeletePostOnlyByPublisher, policyDto.SessionLength,
                                                       policyDto.SessionIdleTime, policyDto.InformOptions,
                                                       policyDto.MinimumPasswordLength, policyDto.ForbiddenWords,
                                                       policyDto.DaysToResetPassword, policyDto.SetQuestion);
      return Ok(string.Format("{0}/policys/{1}", ConfigurationManager.AppSettings["url"], policy.Id));
    }

  }
}