﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace App.API.Controllers
{
  [AllowAnonymous]
  public class IndexController : ApiController
  {
    [Route("")]
    public HttpResponseMessage Get()
    {
      var wat = new List<string>
      {
        "http://cdn.yourepeat.com/media/gif/001/243/751/eb6ffe351aede67cfa4345a812e8ff30.gif",
        "http://i.imgur.com/l6c1xxk.gif",
        "http://tclhost.com/8HUukjF.gif",
        "http://i.imgur.com/bpW6Xkd.gif",
        "http://i.imgur.com/vdLE8dJ.gif",
        "http://tclhost.com/PrLyRs8.gif",
        "http://www.pbh2.com/wordpress/wp-content/uploads/2013/06/funny-gif-grocery-bags.gif",
        "http://wonderchicken.com/randomcrap/yeahwoocurses.gif",
        "https://pbs.twimg.com/profile_images/2727748211/c3d0981ae770f926eedf4eda7505b006.jpeg"
      };
      var random = new Random();
      var sb = new StringBuilder();
      sb.AppendLine("<html>");
      sb.AppendLine("  <head>");
      sb.AppendLine("    <title>Forum Generator Server</title>");
      sb.AppendLine("    <style>body { text-align: center; padding-top: 70px; } </style>");
      sb.AppendLine("  </head>");
      sb.AppendLine("  <body>");
      sb.AppendLine("    <h1>Forum Generator Server is RUNNING</h1>");
      sb.AppendLine("    <img src=\"" + wat[random.Next(wat.Count)] + "\" />");
      sb.AppendLine("  </body>");
      sb.AppendLine("</html>");
      var res = Request.CreateResponse(HttpStatusCode.OK);
      res.Content = new StringContent(sb.ToString(), Encoding.UTF8, "text/html");

      return res;
    }
  }
}