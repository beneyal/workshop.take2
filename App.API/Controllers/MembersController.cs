﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using App.API.DataTransferObjects;
using App.ServiceLayer;

namespace App.API.Controllers
{
  [AllowAnonymous]
  public class MembersController : BaseControllerWithHub
  {
    public MembersController(ServicesBag services) : base(services)
    {
    }

    public IHttpActionResult Get(int id)
    {
      var member = Services.MemberService.GetMember(id);
      if (member != null)
      {
        return Ok(new MemberDto(member));
      }
      return BadRequest("The requested member does not exist.");
    }

    [Route("api/forums/{forumId}/members")]
    public IHttpActionResult Get(int forumId, string name)
    {
      return Ok(new MemberDto(Services.MemberService.GetMember(name, forumId)));
    }

    [Route("api/members/{memberId}/friends")]
    public IHttpActionResult Post(int memberId, [FromBody] int friendId)
    {
      Services.MemberService.AddFriend(memberId, friendId);
      return Ok("member " + friendId + " added as friend of member " + memberId);
    }

    [HttpDelete]
    [Route("api/members/{memberId}/friends")]
    public IHttpActionResult DeleteFriend(int memberId, [FromBody] int friendId)
    {
      Services.MemberService.DeleteFriend(memberId, friendId);
      return Ok("member " + friendId + " deleted from friends list of member " + memberId);
    }

    [HttpGet]
    [Route("api/members/{memberId}/friends")]
    public IHttpActionResult GetFriends(int memberId)
    {
      return Ok(Services.MemberService.GetFriends(memberId).Select(f => new MemberDto(f)).ToList());
    }

    [HttpPost]
    [Route("api/forums/{forumId}/login")]
    public IHttpActionResult Login(int forumId, [FromBody] LoginDetails details)
    {
      var token = Services.MemberService.Login(forumId, details.Name, details.Password);
      if (string.IsNullOrEmpty(token))
        return BadRequest("Incorrect Username or Password");
      return Ok(token);
    }

    [HttpGet]
    [Route("api/logout")]
    public IHttpActionResult Logout(HttpRequestMessage request)
    {
      var token = request.Headers.Authorization.Parameter;
      Services.MemberService.Logout(token);
      return Ok();
    }

    [HttpPost]
    [Route("api/members/{memberID}/ban")]
    public IHttpActionResult BanUser(int memberId)
    {
      Services.MemberService.BanMember(memberId);
      return Ok("member " + memberId + " suspended.");
    }

    [HttpPost]
    [Route("api/members/{memberID}/complain")]
    public IHttpActionResult PublishComplaintOn(int memberId, [FromBody] ComplaintDto complaintDto)
    {
      Services.ComplaintService.Complaint(complaintDto.Content, complaintDto.SubjectId, memberId);
      return Ok("member " + memberId + " complaint on member: " + complaintDto.SubjectId);
    }

    [HttpGet]
    [Route("api/members/{memberID}/complaints")]
    public IHttpActionResult GetComplaints(int memberId)
    {
      if (!Services.MemberService.Matches(Thread.CurrentPrincipal.Identity.Name, memberId) && !IsSuperAdmin())
        return StatusCode(HttpStatusCode.Unauthorized);
      return Ok(Services.ComplaintService.GetComplaints(memberId).Select(c => new ComplaintDto(c)).ToList());
    }

    [HttpGet]
    [Route("api/members/{memberId}/posts")]
    public IHttpActionResult GetPostsByMember(int memberId)
    {
      var member = Services.MemberService.GetMember(memberId);
      if (member != null)
        return Ok(member.Posts.Select(p => new PostDto(p)).ToList());
      return Ok(string.Format("member {0} doesn't exists.", memberId));
    }

    public IHttpActionResult Delete(int memberId)
    {
      if (!Services.MemberService.Matches(Thread.CurrentPrincipal.Identity.Name, memberId) && !IsSuperAdmin())
        return StatusCode(HttpStatusCode.Unauthorized);
      Services.MemberService.DeleteMember(memberId);
      return StatusCode(HttpStatusCode.NoContent);
    }
  }
}
