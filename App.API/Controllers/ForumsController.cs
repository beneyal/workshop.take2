﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web.Http;
using App.API.DataTransferObjects;
using App.Core.Entities;
using App.ServiceLayer;

namespace App.API.Controllers
{
  public class ForumsController : BaseControllerWithHub
  {
    public ForumsController(ServicesBag services) : base(services)
    {
    }

    public IHttpActionResult Post([FromBody] string name)
    {
      if (!IsSuperAdmin())
        return StatusCode(HttpStatusCode.Unauthorized);
      var forum = Services.ForumService.CreateForum(name);
      return
        Ok(string.Format("{0}/forums/{1}",
                         ConfigurationManager.AppSettings["url"], forum.Id));
    }

    public IHttpActionResult Delete([FromUri] int forumId)
    {
      if (!IsSuperAdmin())
        return StatusCode(HttpStatusCode.Unauthorized);
      Services.ForumService.DeleteForum(forumId);
      return StatusCode(HttpStatusCode.NoContent);
    }

    [AllowAnonymous]
    public IHttpActionResult Get([FromUri] string name)
    {
      var forum = Services.ForumService.GetForum(name);
      if (forum != null)
      {
        return Ok(new ForumDto(forum));
      }
      return BadRequest("The requested forum does not exist.");
    }

    [AllowAnonymous]
    public IHttpActionResult Get(int id)
    {
      var forum = Services.ForumService.GetForum(id);
      if (forum != null)
      {
        return Ok(new ForumDto(forum));
      }
      return BadRequest("The requested forum does not exist.");
    }

    [AllowAnonymous]
    public IHttpActionResult Get()
    {
      return Ok(Services.ForumService.GetForums().Select(f => new ForumDto(f)).ToList());
    }

    [AllowAnonymous]
    [HttpPost]
    [Route("api/forums/{forumId}/register")]
    public IHttpActionResult Register(int forumId, [FromBody] RegistrationDetails details)
    {
      if (!ModelState.IsValid)
        return BadRequest("Invalid details");
      var link = string.Format("{0}/confirm?token=", ConfigurationManager.AppSettings["url"]);
      var success = Services.ForumService.Register(forumId,
                                                   details.Name,
                                                   details.Email,
                                                   details.Password,
                                                   details.Question,
                                                   details.Answer,
                                                   link);
      if (success)
        return Ok("A confirmation email has been sent to " + details.Email);
      return BadRequest("A member with email " + details.Email + " already exists.");
    }

    [AllowAnonymous]
    [HttpGet]
    [Route("api/confirm")]
    public IHttpActionResult Confirm(string token)
    {
      Services.MemberService.Confirm(token);
      return Ok("Confirmation Successful");
    }

    [AllowAnonymous]
    [HttpGet]
    [Route("api/forums/administrators/{adminId}")]
    public IHttpActionResult GetAdminForums(int adminId)
    {
      var forums = Services.ForumService.GetAdminForums(adminId);
      var forumList = forums as IList<Forum> ?? forums.ToList();
      if (forumList.Any())
        return Ok(forumList.Select(f => new ForumDto(f)).ToList());
      return Ok(string.Format("member {0} doesn't administrate forums", adminId));
    }

    [Route("api/forums/{forumId}/administrators")]
    public IHttpActionResult Post(int forumId, [FromBody] int memberId)
    {
      if (!IsSuperAdmin() && !IsAdmin(forumId))
        return StatusCode(HttpStatusCode.Unauthorized);
      Services.ForumService.PromoteToAdministrator(forumId, memberId);
      return Ok(string.Format("member {0} added as admin to forum {1}", memberId, forumId));
    }

    [Route("api/forums/{forumId}/administrators/{memberId}")]
    public IHttpActionResult Delete(int forumId, int memberId)
    {
      if (!IsSuperAdmin())
        return StatusCode(HttpStatusCode.Unauthorized);
      Services.ForumService.DemoteAdministrator(forumId, memberId);
      return Ok(string.Format("member {0} removed from forum {1} administrators", memberId, forumId));
    }

//      [HttpGet]
//      [Route("api/forums/{forumId}/moderators")]
//      public IHttpActionResult GetModeratorSubforum(int forumId)
//      {
//        var subforums = Services.SubforumService.GetSubforums(forumId);
//        if (subforums.Any())
//          return Ok(subforums.Select(s => new ModeratorByForumsDTO(s.Id, s.Moderators)).ToList());
//        return Ok(string.Format("member {0} doesn't moderate subforums", moderatorId));
//      }

  }
}