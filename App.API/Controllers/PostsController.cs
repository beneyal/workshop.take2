﻿using System.Configuration;
using System.Net;
using System.Threading;
using System.Web.Http;
using App.API.DataTransferObjects;
using App.Core.Entities;
using App.ServiceLayer;

namespace App.API.Controllers
{
  public class PostsController : BaseControllerWithHub
  {
    public PostsController(ServicesBag services)
      : base(services)
    {
    }

    [AllowAnonymous]
    [Route("api/subforums/{subforumId}/posts")]
    public IHttpActionResult Post([FromUri] int subforumId, [FromBody] PostDto postDto)
    {
      var post = Services.PostService.PublishNewThread(subforumId, postDto.Title, postDto.Content, postDto.PublisherId);
      return Ok(string.Format("{0}/posts/{1}", ConfigurationManager.AppSettings["url"], post.Id));
    }

    [AllowAnonymous]
    [Route("api/posts/{postId}")]
    public IHttpActionResult Post([FromUri] int postId, [FromBody] ReplyPostDto replyDto)
    {
      var reply = Services.PostService.PublishReply(postId, replyDto.Title, replyDto.Content, replyDto.PublisherId);
      return Ok(reply.Id + " added successfully to post " + replyDto.ParentPost);
    }

    [Route("api/posts/{postId}/edit")]
    public IHttpActionResult Post([FromUri] int postId, [FromBody] string content)
    {
      var post = Services.PostService.GetPost(postId);
      var publisherId = post.Publisher.Id;
      var forumId = post.Subforum.ParentForum.Id;
      var subforumId = post.Subforum.Id;
      if (!Services.MemberService.Matches(Thread.CurrentPrincipal.Identity.Name, publisherId) && !IsSuperAdmin() &&
          !IsAdmin(forumId) && !IsModerator(subforumId))
      {
        return StatusCode(HttpStatusCode.Unauthorized);
      }
      Services.PostService.EditPost(postId, content);
      return Ok(postId + " edited successfully.");
    }

    [AllowAnonymous]
    [HttpGet]
    [Route("api/posts/SearchPostsByPublisherId/{publisherId}")]
    public IHttpActionResult GetPostsByPublisherId(int publisherId)
    {
      return Ok(Services.PostService.SearchPostsByPublisherId(publisherId));
    }

    [AllowAnonymous]
    [HttpGet]
    [Route("api/posts/SearchPostsByContent")]
    public IHttpActionResult GetPostsByContent(string content)
    {
      return Ok(Services.PostService.SearchPostsByContent(content));
    }

    [HttpGet]
    [Route("api/posts/{postId}")]
    public IHttpActionResult GetPost(int postId)
    {
      var post = Services.PostService.GetPost(postId);
      var publisherId = post.Publisher.Id;
      var forumId = post.Subforum.ParentForum.Id;
      var subforumId = post.Subforum.Id;
      if (!Services.MemberService.Matches(Thread.CurrentPrincipal.Identity.Name, publisherId) && !IsSuperAdmin() &&
          !IsAdmin(forumId) && !IsModerator(subforumId))
      {
        return StatusCode(HttpStatusCode.Unauthorized);
      }
      return Ok(Services.PostService.GetPost(postId));
    }

    [HttpDelete]
    [Route("api/posts/DeleteThread/{postId}")]
    public IHttpActionResult DeleteThread([FromUri] int postId)
    {
      var post = Services.PostService.GetPost(postId);
      var publisherId = post.Publisher.Id;
      var forumId = post.Subforum.ParentForum.Id;
      var subforumId = post.Subforum.Id;
      if (!Services.MemberService.Matches(Thread.CurrentPrincipal.Identity.Name, publisherId) && !IsSuperAdmin() &&
          !IsAdmin(forumId) && !IsModerator(subforumId))
      {
        return StatusCode(HttpStatusCode.Unauthorized);
      }
      Services.PostService.DeleteThread(postId);
      return Ok(postId + "was successfully deleted.");
    }

    [HttpDelete]
    [Route("api/posts/DeleteReplyPost/{postId}")]
    public IHttpActionResult DeleteReplyPost([FromUri] int postId)
    {
      var post = Services.PostService.GetPost(postId);
      var publisherId = post.Publisher.Id;
      var forumId = post.Subforum.ParentForum.Id;
      var subforumId = post.Subforum.Id;
      if (!Services.MemberService.Matches(Thread.CurrentPrincipal.Identity.Name, publisherId) && !IsSuperAdmin() &&
          !IsAdmin(forumId) && !IsModerator(subforumId))
      {
        return StatusCode(HttpStatusCode.Unauthorized);
      }
      Services.PostService.DeleteReplyPost(postId);
      return Ok(postId + "was successfully deleted.");
    }
  }
}