﻿using System.Collections.Generic;
using App.Core.Entities;
using App.Core.Interfaces.Models;

namespace App.Core.Interfaces.Managers
{
  public interface IPostManager : IManager
  {
    void AddPost(IPostModel newPost, ISubforumModel subforum);
    void AddReplyPost(IPostModel newReplyPost, IPostModel newPost);
    void DeletePost(int postId);
    void DeleteReplyPost(int id);
    IPostModel GetPost(int id);
    IPostModel GetPostByPublisherId(int publisherId);
    IEnumerable<Post> GetPostByContent(string content);
  }
}
