﻿using System;
using System.Collections.Generic;
using App.Core.Entities;
using App.Core.Interfaces.Models;

namespace App.Core.Interfaces.Managers
{
  public interface IPolicyManager : IManager
  {
    void AddPolicy(IPolicyModel newPolicy, IForumModel forum);
    bool CheckedMaximumModeratorAmount(int forumId);
    bool CheckedMaximumAdministratorAmount(int forumId);
    bool CheckedPostsToBecomeModerator(int subforum, int memberId);
    bool CheckedPostsToBecomeAdministrator(int forumId, int memberId);
    bool CheckedPostsToBecomeGoldMember(int memberId);
    bool CheckedPostsToBecomeSilverMember(int memberId);
    bool CheckedDaysToBecomeModerator(int memberId);
    bool CheckedDaysToBecomeAdministrator(int memberId);
    void CheckedDaysToBecomeSilverMember();
    void CheckedDaysToBecomeGoldMember();
    bool CheckedMinimumPasswordLength(int forumId, string password);
    bool ForbiddenWords(List<String> words, string content);
    bool SetQuestion(int forumId);
  }
}
