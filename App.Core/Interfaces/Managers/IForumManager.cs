﻿using System.Collections.Generic;
using App.Core.Entities;
using App.Core.Interfaces.Models;

namespace App.Core.Interfaces.Managers
{
  public interface IForumManager : IManager
  {
    void AddForum(IForumModel newForum);
    void DeleteForum(int forumId);
    void DeleteForum(string forumName);
    IEnumerable<Forum> GetAll();
    IForumModel GetForum(int id);
    IForumModel GetForum(string forumName);
  }
}