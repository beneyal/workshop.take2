﻿using System.Collections.Generic;
using App.Core.Interfaces.Models;

namespace App.Core.Interfaces.Managers
{
  public interface IRoleManager : IManager
  {
    void AddMemberType(IForumModel forum, string memberType);
    void RemoveMemberType(IForumModel forum, string memberType);
    void PromoteMemberToAdmin(IMemberModel member, IForumModel forum);
    void RevokeAdminRole(IMemberModel member, IForumModel forum);
    void PromoteMemberToModerator(IMemberModel member, ISubforumModel subforum);
    void RevokeModeratorRole(IMemberModel member, ISubforumModel subforum);
    IEnumerable<IMemberModel> GetSuperAdmins();
    IEnumerable<IMemberModel> GetSuperAdminsAsNewMembers(IForumModel forum);
    IEnumerable<IForumModel> GetAdminForums(int adminId);
    IEnumerable<ISubforumModel> GetModeratorSubforums(int moderatorId);
  }
}