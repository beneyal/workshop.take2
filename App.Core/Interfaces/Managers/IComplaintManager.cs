﻿using System.Collections.Generic;
using App.Core.Entities;
using App.Core.Interfaces.Models;

namespace App.Core.Interfaces.Managers
{
  public interface IComplaintManager : IManager
  {
    void AddComplaint(IComplaintModel complaint);
    IEnumerable<Complaint> GetBySubjectId(int memberId);
  }
}