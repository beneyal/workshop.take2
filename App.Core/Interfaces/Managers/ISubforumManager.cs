﻿using System.Collections.Generic;
using App.Core.Entities;
using App.Core.Interfaces.Models;

namespace App.Core.Interfaces.Managers
{
  public interface ISubforumManager : IManager
  {
    void AddSubforum(ISubforumModel subforum, IForumModel parent);
    void DeleteSubforum(int subforumId);
    void DeleteSubforum(string subject);
    IEnumerable<Subforum> GetByForumId(int forumId);
    ISubforumModel GetSubforum(int id);
    ISubforumModel GetSubforum(string name);
  }
}