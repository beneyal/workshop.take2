﻿using System.Collections.Generic;
using App.Core.Entities;
using App.Core.Interfaces.Models;

namespace App.Core.Interfaces.Managers
{
  public interface IMemberManager : IManager
  {
    void AddMember(IMemberModel newMember);
    void DeleteMember(int memberId);
    IEnumerable<Member> GetAll();
    IMemberModel GetMember(string memberName, int forumId);
    void Confirm(string name, string email);
    void Logout(string token);
    IMemberModel GetMember(int id);
    Member AddFriend(int memberId, int friendId);
    ICollection<Member> GetFriends(int memberId);
    void DeleteFriend(int memberId, int friendId);
    void BanMember(int memberId);
    bool Exists(string email);
    bool Matches(string requestEmail, int requestedMemberId);
  }
}