﻿using System.Threading.Tasks;
using App.Core.Interfaces.Models;

namespace App.Core.Interfaces.Managers
{
  public interface IEmailManager
  {
    void SendConfirmationRequest(IMemberModel member, string confirmationLink);
  }
}