﻿namespace App.Core.Interfaces.Managers
{
  public interface IManager
  {
    IContext Context { get; set; }
  }
}