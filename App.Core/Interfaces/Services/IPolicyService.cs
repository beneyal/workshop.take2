﻿using System;
using System.Collections.Generic;
using System.Timers;
using App.Core.Entities;

namespace App.Core.Interfaces.Services
{
  public interface IPolicyService
  {
    Policy SetNewPolicy(int forumId, int id, int maximumAdministratorAmount, int maximumModeratorAmount,
      int daysToBecomeModerator,int postsToBecomeModerator, int daysToBecomeAdministrator, int postsToBecomeAdministrator,
      int daysToBecomeGoldMember, int postsToBecomeGoldMember, int daysToBecomeSilverMember, int postsToBecomeSilverMember,
      bool deletePostOnlyByAdministrator, bool deletePostOnlyByPublisher, int sessionLength, int sessionIdleTime, Policy.InformOption informOptions,
      int minimumPasswordLength, List<String> forbiddenWords, int daysToResetPassword, bool setQuestion);
    bool CheckedMaximumModeratorAmount(int forumId);
    bool CheckedMaximumAdministratorAmount(int forumId);
    bool CheckedPostsToBecomeModerator(int subforumId, int memberId);
    bool CheckedPostsToBecomeAdministrator(int forumId, int memberId);
    bool CheckedPostsToBecomeGoldMember(int memberId);
    bool CheckedPostsToBecomeSilverMember(int memberId);
    bool CheckedDaysToBecomeModerator(int memberId);
    bool CheckedDaysToBecomeAdministrator(int memberId);
    void CheckedDaysToBecomeSilverMember(object s, ElapsedEventArgs e);
    void CheckedDaysToBecomeGoldMember(object s, ElapsedEventArgs e);
    bool CheckedMinimumPasswordLength(int forumId, string password);
    bool ForbiddenWords(List<String> words, string content);
    bool SetQuestion(int forumId);
  }
}