﻿using System.Collections.Generic;
using App.Core.Entities;

namespace App.Core.Interfaces.Services
{
  public interface ISubforumService
  {
    IEnumerable<Subforum> GetSubforums(int forumId);
    Subforum GetSubforum(int subforumId);
    Subforum GetSubforum(string subforumName);
    Subforum CreateSubforum(int forumId, string subject);
    void DeleteSubforum(int subforumId);
    void DeleteSubforum(string subject);
    IEnumerable<Subforum> GetModeratorSubforums(int moderatorId);
    void PromoteToModerator(int subforumId, int memberId);
    void DemoteModerator(int subforumId, int moderatorId);
  }
}