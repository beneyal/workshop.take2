﻿using System.Collections.Generic;
using App.Core.Entities;

namespace App.Core.Interfaces.Services
{
  public interface IComplaintService
  {
    void Complaint(string content, int subjectId, int memberId);
    IEnumerable<Complaint> GetComplaints(int memberId);
  }
}