﻿using System.Collections.Generic;
using App.Core.Entities;

namespace App.Core.Interfaces.Services
{
  public interface IPostService
  {
    Post PublishNewThread(int subforumId, string title, string content, int publisherId);
    Post GetPost(int postId);
    Post SearchPostsByPublisherId(int publisherId);
    void DeleteThread(int threadId);
    void EditPost(int postId, string content);
    void DeleteReplyPost(int postId);
    ReplyPost PublishReply(int postId, string content, string title, int publisherId);
    IEnumerable<Post> SearchPostsByContent(string content);
  }
}