﻿namespace App.Core.Interfaces.Services
{
  public interface IGuestService
  {
    void Register(int forumId, string username, string password, string email,
                  string uri = null);

    void ConfirmRegistration(int forumId, string username, string password,
                             string email);

    void LoginAsGuest();
  }
}