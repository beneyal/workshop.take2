﻿namespace App.Core.Interfaces.Services
{
  public interface IReportService
  {
    void GenerateSuperAdminReport();
    void GenerateAdministratorReport();
  }
}