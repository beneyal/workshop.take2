﻿using System.Collections.Generic;
using System.Threading.Tasks;
using App.Core.Entities;

namespace App.Core.Interfaces.Services
{
  public interface IForumService
  {
    void AddNewMemberType(string type);
    void DeleteMemberType(int typeId);
    void SetForumPolicy();
    Forum CreateForum(string newForumName);
    IEnumerable<Forum> GetForums();
    Forum GetForum(int id);
    Forum GetForum(string forumName);
    void DeleteForum(int forumId);
    void DeleteForum(string forumName);
    bool Register(int forumId, string name, string email, string password, string question, string answer,
                  string confirmationLink);
    IEnumerable<Forum> GetAdminForums(int adminId);
    void PromoteToAdministrator(int forumId, int memberId);
    void DemoteAdministrator(int forumId, int adminId);
  }
}