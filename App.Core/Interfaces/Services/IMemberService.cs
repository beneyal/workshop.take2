﻿using System.Collections.Generic;
using App.Core.Entities;

namespace App.Core.Interfaces.Services
{
  public interface IMemberService
  {
    string Login(int forumId, string username, string password);
    void Logout(string token);
    void SetMemberType(int forumId, int memberId, int typeId);
    void PromoteToModerator(int subforumId, int memberId);
    void DemoteModerator(int subforumId, int moderatorId);
    void PromoteToAdministrator(int forumId, int memberId);
    void DemoteAdministrator(int forumId, int adminId);
    Member GetMember(int id);
    Member GetMember(string name, int forumId);
    void DeleteMember(int id);
    void Confirm(string token);
    void AddFriend(int memberId, int friendId);
    ICollection<Member> GetFriends(int memberId);
    void DeleteFriend(int memberId, int friendId);
    void BanMember(int memberId);
    bool Exists(string email);
    bool Matches(string requestEmail, int requestedMemberId);
  }
}