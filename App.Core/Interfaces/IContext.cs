﻿using System;
using System.Data.Entity;
using App.Core.Entities;

namespace App.Core.Interfaces
{
  public interface IContext : IDisposable
  {
    DbSet<Member> Members { get; set; }
    DbSet<Forum> Forums { get; set; }
    DbSet<Subforum> Subforums { get; set; }
    DbSet<Post> Posts { get; set; }
    DbSet<Role> Roles { get; set; }
    DbSet<Policy> Policies { get; set; }
    DbSet<Complaint> Complaints { get; set; }
    int SaveChanges();
  }
}