using App.Core.Interfaces.Managers;

namespace App.Core.Interfaces
{
  public interface IManagersProvider
  {
    IForumManager ForumManager { get; }
    ISuperAdminManager SuperAdminManager { get; }
  }
}