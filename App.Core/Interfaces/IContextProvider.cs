﻿namespace App.Core.Interfaces
{
  public interface IContextProvider
  {
    IContext NewContext();
  }
}