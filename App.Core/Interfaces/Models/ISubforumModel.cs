﻿using App.Core.Entities;

namespace App.Core.Interfaces.Models
{
  public interface ISubforumModel
  {
    Subforum Subforum { get; }
    void SetParent(IForumModel parent);
    void AddAdministratorsAsModerators(ForumRole adminRole, SubforumRole moderatorRole);
  }
}