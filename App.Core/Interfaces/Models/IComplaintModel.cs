﻿using App.Core.Entities;

namespace App.Core.Interfaces.Models
{
  public interface IComplaintModel
  {
    Complaint Complaint { get; }

  }
}