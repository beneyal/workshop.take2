﻿using App.Core.Entities;

namespace App.Core.Interfaces.Models
{
  public interface IForumModel
  {
    Forum Forum { get; }
    void AddMember(IMemberModel member);
    void DeleteMember(IMemberModel member);
    IMemberModel GetMember(string username);
    void AddSuperAdminAsAdministrator();
    void AddSubforum(ISubforumModel subforum);
  }
}