﻿using App.Core.Entities;

namespace App.Core.Interfaces.Models
{
  public interface IPostModel
  {
    Post Post { get; }

  }
}