﻿using System;
using App.Core.Entities;

namespace App.Core.Interfaces.Models
{
  public interface IMemberModel
  {
    Member Member { get; }
    void PromoteToAdmin(ForumRole adminRole);
    void PromoteToModerator(SubforumRole moderatorRole);
    string GenerateConfirmationToken();
    string Login();
    DateTime GetLoginTime();
  }
}