﻿using App.Core.Entities;

namespace App.Core.Interfaces.Models
{
  public interface IPolicyModel
  {
    Policy Policy { get; }

  }
}