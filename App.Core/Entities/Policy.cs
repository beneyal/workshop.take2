﻿using System;
using System.Collections.Generic;

namespace App.Core.Entities
{
  public class Policy
  {
   
    public enum InformOption
    {
      Online,
      Offline
    };

    public Policy()
    {
      ForbiddenWords = new List<String>();
    }

    public Forum Forum { get; set; }
    // forum options
    public int Id { get; set; }
    public int MaximumAdministratorAmount { get; set; }
    public int MaximumModeratorAmount { get; set; }
    public int DaysToBecomeModerator { get; set; }
    public int PostsToBecomeModerator { get; set; }
    public int DaysToBecomeAdministrator { get; set; }
    public int PostsToBecomeAdministrator { get; set; }
    public int DaysToBecomeGoldMember { get; set; }
    public int PostsToBecomeGoldMember { get; set; }
    public int DaysToBecomeSilverMember { get; set; }
    public int PostsToBecomeSilverMember { get; set; }
    public bool DeletePostOnlyByAdministrator { get; set; }
    public bool DeletePostOnlyByPublisher { get; set; }
    public int SessionLength { get; set; }
    public int SessionIdleTime { get; set; }
    public InformOption InformOptions { get; set; }

    // security options
    public int MinimumPasswordLength { get; set; }
    public List<String> ForbiddenWords { get; set; }
    public int DaysToResetPassword { get; set; }
    public bool SetQuestion { get; set; }
  }
}