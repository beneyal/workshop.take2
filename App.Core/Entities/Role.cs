﻿using System;
using System.Collections.Generic;

namespace App.Core.Entities
{
  public class Role
  {
    public Role()
    {
      Members = new List<Member>();
      RoleSince = DateTime.Now;
    }

    public string Name { get; set; }
    public int Id { get; set; }
    public ICollection<Member> Members { get; set; }
    public DateTime RoleSince { get; set; }

    public override string ToString()
    {
      return Name;
    }
  }
}