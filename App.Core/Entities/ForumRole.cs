namespace App.Core.Entities
{
  public class ForumRole : Role
  {
    public Forum Forum { get; set; }
    public override string ToString()
    {
      return Name + ":" + Forum.Id;
    }
  }
}