﻿using System.Collections.Generic;

namespace App.Core.Entities
{
  public class Subforum
  {
    public Subforum()
    {
      Moderators = new List<Member>();
      Posts = new List<Post>();
    }

    public string Subject { get; set; }
    public Forum ParentForum { get; set; }
    public ICollection<Member> Moderators { get; private set; }
    public ICollection<Post> Posts { get; private set; }
    public int Id { get; set; }
  }
}