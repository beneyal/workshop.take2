﻿namespace App.Core.Entities
{
  public class Complaint
  {
    public string Content { get; set; }
    public int SubjectId { get; set; }
    public int FiledById { get; set; }
    public int Id { get; set; }
  }
}