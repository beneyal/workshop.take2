﻿using System;
using System.Collections.Generic;

namespace App.Core.Entities
{
  public class Member
  {
    public Member()
    {
      Friends = new List<Member>();
      Posts = new List<Post>();
      Roles = new List<Role>();
      IsActive = true;
      IsLoggedIn = false;
      IsConfirmed = false;
      MemberSince = DateTime.Now;
      Token = string.Empty;
      BanTime = DateTime.MaxValue;
      ModeratorSince = DateTime.MaxValue;
    }

    public string Name { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
    public string SecurityQuestion { get; set; }
    public string SecurityAnswer { get; set; }
    public bool IsActive { get; set; }
    public DateTime BanTime { get; set; }
    public bool IsLoggedIn { get; set; }
    public string Token { get; set; }
    public bool IsConfirmed { get; set; }
    public DateTime MemberSince { get; set; }
    public Forum Forum { get; set; }
    public ICollection<Member> Friends { get; private set; }
    public ICollection<Post> Posts { get; private set; }
    public ICollection<Role> Roles { get; set; }
    public int Id { get; set; }
    public DateTime ModeratorSince { get; set; }
    public Member AppointToModeratorBy { get; set; }
  }
}