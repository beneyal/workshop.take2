﻿using System;
using System.Collections.Generic;

namespace App.Core.Entities
{
  public class Post
  {
     
    public Post()
    {
      Replies = new List<ReplyPost>();
      PublishedOn = DateTime.Now;
    }

    public string Title { get; set; }
    public string Content { get; set; }
    public DateTime PublishedOn { get; set; }
    public Member Publisher { get; set; }
    public Subforum Subforum { get; set; }
    public ICollection<ReplyPost> Replies { get; private set; }
    public int Id { get; set; }
  }
}