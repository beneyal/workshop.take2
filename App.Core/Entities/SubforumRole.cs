﻿namespace App.Core.Entities
{
  public class SubforumRole : Role
  {
    public Subforum Subforum { get; set; }
    public override string ToString()
    {
      return Name + ":" + Subforum.Id;
    }
  }
}