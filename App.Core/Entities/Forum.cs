﻿using System.Collections.Generic;

namespace App.Core.Entities
{
  public class Forum
  {
    public Forum()
    {
      Members = new List<Member>();
      Subforums = new List<Subforum>();
    }

    public string Name { get; set; }
    public ICollection<Member> Members { get; private set; }
    public ICollection<Subforum> Subforums { get; private set; }
    public int Id { get; set; }
    public Policy Policy { get; set; }
  }
}