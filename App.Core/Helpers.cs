﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using App.Core.Entities;
using App.Core.Interfaces;

namespace App.Core
{
  public static class Roles
  {
    public const string SuperAdmin = "SuperAdmin";
    public const string Admin = "Admin";
    public const string Moderator = "Moderator";
    public const string Gold = "Gold";
    public const string Silver = "Silver";
    public const string Regular = "Regular";
    public const string Member = "wat";
    public const string Guest = "Guest";

    public static bool IsPredefinedRole(string role)
    {
      var roles = new List<string> { SuperAdmin, Admin, Moderator, Gold, Silver, Regular, Guest };
      return roles.Any(r => r.Equals(role, StringComparison.OrdinalIgnoreCase));
    }
  }

  public static class Extensions
  {
    public static ForumRole GetAdminRole(this IContext context, Forum forum)
    {
      return context.Roles.OfType<ForumRole>().SingleOrDefault(r => r.Forum.Id == forum.Id && r.Name == Roles.Admin);
    }

    public static SubforumRole GetModeratorRole(this IContext context, Subforum subforum)
    {
      return
        context.Roles.OfType<SubforumRole>()
          .SingleOrDefault(r => r.Subforum.Id == subforum.Id && r.Name == Roles.Moderator);
    }

    public static IEnumerable<Forum> GetForums(this IContext context)
    {
      return
        context.Forums.Include(f => f.Members)
          .Include(f => f.Members.Select(m => m.Roles))
          .Include(f => f.Subforums)
          .ToList();
    }

   
    public static Post GetPost(this IContext context, int id)
    {
      return
        context.Posts
          .Include(p => p.Publisher)
          .Include(p => p.Subforum)
          .Include(p => p.Replies)
          .Include(p => p.Replies.Select(r => r.Publisher))
          .Include(p => p.Replies.Select(r => r.ParentPost))
          .Include(p => p.Replies.Select(r => r.Replies))
          .Include(p => p.Replies.Select(r => r.Subforum))
          .SingleOrDefault(p => p.Id == id);
    }

    public static ReplyPost GetReply(this IContext context, int id)
    {
      return
        context.Posts.OfType<ReplyPost>()
          .Include(p => p.Publisher)
          .Include(p => p.Subforum)
          .Include(p => p.Replies)
          .Include(p => p.ParentPost)
          .Include(p => p.Replies.Select(r => r.Publisher))
          .Include(p => p.Replies.Select(r => r.ParentPost))
          .Include(p => p.Replies.Select(r => r.Replies))
          .Include(p => p.Replies.Select(r => r.Subforum))
          .SingleOrDefault(p => p.Id == id);
    }

    public static Subforum GetSubforum(this IContext context, int id)
    {
      return
        context.Subforums.Where(s => s.Id == id)
          .Include(s => s.ParentForum)
          .Include(s => s.Moderators)
          .Include(s => s.Posts)
          .Include(s => s.Posts.Select(p => p.Publisher))
          .Include(s => s.Posts.Select(p => p.Replies))
          .SingleOrDefault();
    }

    public static Member GetMember(this IContext context, int id)
    {
      return context.GetQueryableMember(id).SingleOrDefault();
    }

    public static IQueryable<Member> GetQueryableMember(this IContext context, int id)
    {
      return
        context.Members.Where(m => m.Id == id)
          .Include(m => m.Forum)
          .Include(m => m.Forum.Subforums)
          .Include(m => m.Friends)
          .Include(m => m.Friends.Select(f => f.Forum))
          .Include(m => m.Roles)
          .Include(m => m.Posts);
    }

    public static Role GetSuperAdminRole(this IContext context)
    {
      return context.Roles.Include(r => r.Members).Single(r => r.Name == Roles.SuperAdmin);
    }

    public static string[] GetRolesArray(this IContext context, int memberId)
    {
      var forumRoles = context.Roles.Include(r => r.Members).OfType<ForumRole>().Include(r => r.Forum).ToList();
      var subforumRoles = context.Roles.Include(r => r.Members).OfType<SubforumRole>().Include(r => r.Subforum).ToList();
      var superAdminRole = context.GetSuperAdminRole();
      var allRoles = new List<Role>();
      Action<Role> action = r =>
                            {
                              if (r.Members.Select(m => m.Id).Contains(memberId))
                                allRoles.Add(r);
                            };
      forumRoles.ForEach(action);
      subforumRoles.ForEach(action);
      action(superAdminRole);
      return allRoles.Select(r => r.ToString()).ToArray();
    }

    public static string Hash(this string password)
    {
      var rng = new RNGCryptoServiceProvider();
      var salt = new byte[24];
      rng.GetBytes(salt);
      var hash = new Rfc2898DeriveBytes(password, salt).GetBytes(24);
      return string.Format("{0}:{1}", Convert.ToBase64String(salt), Convert.ToBase64String(hash));
    }

    public static bool VerifyPassword(this string password, string correctHash)
    {
      var salt = Convert.FromBase64String(correctHash.Split(':')[0]);
      var hash = Convert.FromBase64String(correctHash.Split(':')[1]);
      var hashedPassword = Convert.ToBase64String(new Rfc2898DeriveBytes(password, salt).GetBytes(24));
      return hashedPassword == Convert.ToBase64String(hash);
    }

    public static IEnumerable<Forum> GetAdminForums(this IContext context, int adminId)
    {
      var member = context.GetMember(adminId);
      return member.Roles.OfType<ForumRole>().Where(r => r.Name == Roles.Admin).Select(f => f.Forum);
    }

    public static IEnumerable<Subforum> GetModeratorSubforums(this IContext context, int moderatorId)
    {
      var member = context.GetMember(moderatorId);
      return member.Roles.OfType<SubforumRole>().Where(r => r.Name == Roles.Moderator).Select(s => s.Subforum);
    }
  }
}