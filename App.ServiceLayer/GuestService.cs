﻿using App.Core.Interfaces.Services;
using App.DomainLayer.Helpers;

namespace App.ServiceLayer
{
  public class GuestService : BaseService, IGuestService
  {
    public GuestService(ManagersProvider managers) : base(managers)
    {
    }

    public void Register(int forumId, string username, string password,
                         string email, string uri = null)
    {
    }

    public void ConfirmRegistration(int forumId, string username,
                                    string password,
                                    string email)
    {
    }

    public void LoginAsGuest()
    {
      // Need to think about the implications of being a guest
    }
  }
}