﻿using App.Core.Interfaces.Services;
using App.DomainLayer.Helpers;

namespace App.ServiceLayer
{
  public class ForumGeneratorService : BaseService, IForumGeneratorService
  {
    public ForumGeneratorService(ManagersProvider managers) : base(managers)
    {
    }
  }
}