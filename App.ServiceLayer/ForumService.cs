﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Core;
using App.Core.Entities;
using App.Core.Interfaces.Models;
using App.Core.Interfaces.Services;
using App.DomainLayer.Helpers;
using App.DomainLayer.Models;

namespace App.ServiceLayer
{
  public class ForumService : BaseService, IForumService
  {
    public ForumService(ManagersProvider managers) : base(managers)
    {
    }

    public void AddNewMemberType(string type)
    {
      throw new System.NotImplementedException();
    }

    public void DeleteMemberType(int typeId)
    {
    }

    public void SetForumPolicy()
    {
    }

    public Forum CreateForum(string newForumName)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        IForumModel forum = new ForumModel(new Forum { Name = newForumName });
        Managers.ForumManager.AddForum(forum);
        var superAdmins = Managers.RoleManager.GetSuperAdmins();
        foreach (var superAdmin in superAdmins)
        {
          Managers.RoleManager.PromoteMemberToAdmin(superAdmin, forum);
          forum.AddMember(superAdmin);
        }
        uow.SaveChanges();
        return forum.Forum;
      }
    }

    public void DeleteForum(int forumId)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        Managers.ForumManager.DeleteForum(forumId);
        uow.SaveChanges();
      }
    }

    public void DeleteForum(string forumName)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        Managers.ForumManager.DeleteForum(forumName);
        uow.SaveChanges();
      }
    }

    public bool Register(int forumId, string name, string email, string password, string question, string answer,
                         string confirmationLink)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        if (Managers.MemberManager.Exists(email))
          return false;
        var forum = Managers.ForumManager.GetForum(forumId);
        var newMember = new MemberModel(new Member
        {
          Name = name,
          Email = email,
          Password = password.Hash(),
          SecurityQuestion = question,
          SecurityAnswer = answer,
          Forum = forum.Forum,
          IsLoggedIn = false,
          IsActive = false,
          IsConfirmed = false
        });
        forum.AddMember(newMember);
        uow.SaveChanges();
        Managers.EmailManager.SendConfirmationRequest(newMember, confirmationLink);
        return true;
      }
    }

    public IEnumerable<Forum> GetForums()
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.ForumManager.GetAll();
      }
    }

    public Forum GetForum(int id)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.ForumManager.GetForum(id).Forum;
      }
    }

    public Forum GetForum(string forumName)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.ForumManager.GetForum(forumName).Forum;
      }
    }

    public IEnumerable<Forum> GetAdminForums(int adminId)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        return Managers.RoleManager.GetAdminForums(adminId).Select(f => f.Forum);
      }
    }

    public void PromoteToAdministrator(int forumId, int memberId)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        var member = Managers.MemberManager.GetMember(memberId);
        var forum = Managers.ForumManager.GetForum(forumId);
        Managers.RoleManager.PromoteMemberToAdmin(member, forum);
        uow.SaveChanges();
      }
    }

    public void DemoteAdministrator(int forumId, int adminId)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        var member = Managers.MemberManager.GetMember(adminId);
        var forum = Managers.ForumManager.GetForum(forumId);
        Managers.RoleManager.RevokeAdminRole(member, forum);
        uow.SaveChanges();
      }
    }
  }
}