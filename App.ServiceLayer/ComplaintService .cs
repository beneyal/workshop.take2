﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using App.Core;
using App.Core.Entities;
using App.Core.Interfaces.Models;
using App.Core.Interfaces.Services;
using App.DomainLayer.Helpers;
using App.DomainLayer.Models;

namespace App.ServiceLayer
{
  public class ComplaintService : BaseService, IComplaintService
  {
    public ComplaintService(ManagersProvider managers) : base(managers)
    {
    }

    public void Complaint(string content, int subjectId, int memberId)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        var newComplaint = new ComplaintModel(new Complaint
        {
          Content = content,
          SubjectId = subjectId,
          FiledById = memberId
        });
        Managers.ComplaintManager.AddComplaint(newComplaint);
        uow.SaveChanges();
      }
    }

    public IEnumerable<Complaint> GetComplaints(int memberId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.ComplaintManager.GetBySubjectId(memberId);
      }
    }
  }
}