﻿using App.Core.Interfaces.Services;

namespace App.ServiceLayer
{
  public class ServicesBag
  {
    public ServicesBag(IForumGeneratorService forumGeneratorService,
                       IForumService forumService, IMemberService memberService,
                       IPostService postService, ISubforumService subforumService,
      IGuestService guestService, IReportService reportService, IPolicyService policyService, IComplaintService complaintService)
    {
      ForumGeneratorService = forumGeneratorService;
      ForumService = forumService;
      MemberService = memberService;
      PostService = postService;
      SubforumService = subforumService;
      GuestService = guestService;
      ReportService = reportService;
      PolicyService = policyService;
      ComplaintService = complaintService;
    }

    public IForumGeneratorService ForumGeneratorService { get; private set; }
    public IForumService ForumService { get; private set; }
    public IMemberService MemberService { get; private set; }
    public IPostService PostService { get; private set; }
    public ISubforumService SubforumService { get; private set; }
    public IGuestService GuestService { get; private set; }
    public IReportService ReportService { get; private set; }
    public IPolicyService PolicyService { get; set; }
    public IComplaintService ComplaintService { get; private set; }
  }
}