﻿using System;
using System.Collections.Generic;
using System.Timers;
using App.Core.Entities;
using App.Core.Interfaces.Models;
using App.Core.Interfaces.Services;
using App.DomainLayer.Helpers;
using App.DomainLayer.Models;

namespace App.ServiceLayer
{
  public class PolicyService : BaseService, IPolicyService
  {
    public PolicyService(ManagersProvider managers)
      : base(managers)
    {
      var timer = new Timer { Interval = TimeSpan.FromDays(1).TotalMilliseconds };
      timer.Elapsed += CheckedDaysToBecomeSilverMember;
    }

    public Policy SetNewPolicy(int forumId, int id, int maximumAdministratorAmount, int maximumModeratorAmount,
                               int daysToBecomeModerator,
                               int postsToBecomeModerator, int daysToBecomeAdministrator, int postsToBecomeAdministrator,
                               int daysToBecomeGoldMember,
                               int postsToBecomeGoldMember, int daysToBecomeSilverMember, int postsToBecomeSilverMember,
                               bool deletePostOnlyByAdministrator,
                               bool deletePostOnlyByPublisher, int sessionLength, int sessionIdleTime,
                               Policy.InformOption informOptions,
                               int minimumPasswordLength, List<string> forbiddenWords, int daysToResetPassword,
                               bool setQuestion)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        var forum = Managers.ForumManager.GetForum(forumId);
        IPolicyModel policy =
          new PolicyModel(new Policy
          {
            Id = id,
            Forum = forum.Forum,
            MaximumAdministratorAmount = maximumAdministratorAmount,
            MaximumModeratorAmount = maximumModeratorAmount,
            DaysToBecomeModerator = daysToBecomeModerator,
            PostsToBecomeModerator = postsToBecomeModerator,
            DaysToBecomeAdministrator = daysToBecomeAdministrator,
            PostsToBecomeAdministrator = postsToBecomeAdministrator,
            DaysToBecomeGoldMember = daysToBecomeGoldMember,
            PostsToBecomeGoldMember = postsToBecomeGoldMember,
            DaysToBecomeSilverMember = daysToBecomeSilverMember,
            PostsToBecomeSilverMember = postsToBecomeSilverMember,
            DeletePostOnlyByAdministrator = deletePostOnlyByAdministrator,
            DeletePostOnlyByPublisher = deletePostOnlyByPublisher,
            SessionLength = sessionLength,
            SessionIdleTime = sessionIdleTime,
            InformOptions = informOptions,
            MinimumPasswordLength = minimumPasswordLength,
            ForbiddenWords = forbiddenWords,
            DaysToResetPassword = daysToResetPassword,
            SetQuestion = setQuestion
          });
        Managers.PolicyManager.AddPolicy(policy, forum);
        uow.SaveChanges();
        return policy.Policy;
      }
    }

    public bool CheckedMaximumModeratorAmount(int forumId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.PolicyManager.CheckedMaximumModeratorAmount(forumId);
      }
    }

    public bool CheckedMaximumAdministratorAmount(int forumId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.PolicyManager.CheckedMaximumAdministratorAmount(forumId);
      }
    }

    public bool CheckedPostsToBecomeModerator(int subforumId, int memberId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.PolicyManager.CheckedPostsToBecomeModerator(subforumId, memberId);
      }
    }

    public bool CheckedPostsToBecomeAdministrator(int forumId, int memberId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.PolicyManager.CheckedPostsToBecomeAdministrator(forumId, memberId);
      }
    }

    public bool CheckedPostsToBecomeGoldMember(int memberId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.PolicyManager.CheckedPostsToBecomeGoldMember(memberId);
      }
    }

    public bool CheckedPostsToBecomeSilverMember(int memberId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.PolicyManager.CheckedPostsToBecomeSilverMember(memberId);
      }
    }

    public bool CheckedDaysToBecomeModerator(int memberId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.PolicyManager.CheckedDaysToBecomeModerator(memberId);
      }
    }

    public bool CheckedDaysToBecomeAdministrator(int memberId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.PolicyManager.CheckedDaysToBecomeAdministrator(memberId);
      }
    }

    public void CheckedDaysToBecomeSilverMember(object s, ElapsedEventArgs e)
    {
      using (Managers.NewUnitOfWork())
      {
        Managers.PolicyManager.CheckedDaysToBecomeSilverMember();
      }
    }

    public void CheckedDaysToBecomeGoldMember(object s, ElapsedEventArgs e)
    {
      using (Managers.NewUnitOfWork())
      {
        Managers.PolicyManager.CheckedDaysToBecomeGoldMember();
      }
    }

    public bool CheckedMinimumPasswordLength(int forumId, string password)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.PolicyManager.CheckedMinimumPasswordLength(forumId, password);
      }
    }

    public bool ForbiddenWords(List<string> words, string content)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.PolicyManager.ForbiddenWords(words, content);
      }
    }

    public bool SetQuestion(int forumId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.PolicyManager.SetQuestion(forumId);
      }
    }

  }
}