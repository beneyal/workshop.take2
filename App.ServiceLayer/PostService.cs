﻿using System;
using System.Runtime.Remoting.Contexts;
using App.Core.Entities;
using App.Core.Interfaces.Models;
using App.Core.Interfaces.Services;
using App.DomainLayer.Helpers;
using App.DomainLayer.Models;
using System.Collections.Generic;
using System.Linq;
using App.Core.Interfaces;


namespace App.ServiceLayer
{
  public class PostService : BaseService, IPostService
  {
    public PostService(ManagersProvider managers)
      : base(managers)
    {
    }

    public Post GetPost(int postId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.PostManager.GetPost(postId).Post;
      }
    }

    public Post SearchPostsByPublisherId(int publisherId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.PostManager.GetPostByPublisherId(publisherId).Post;
      }
    }

    public Post PublishNewThread(int subforumId, string title, string content, int publisherId)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        var subforum = Managers.SubforumManager.GetSubforum(subforumId);
        var member = Managers.MemberManager.GetMember(publisherId);
        IPostModel post =
          new PostModel(new Post
                        {
                          Title = title,
                          Content = content,
                          Publisher = member.Member
                        });
        Managers.PostManager.AddPost(post, subforum);
        uow.SaveChanges();
        return post.Post;
      }
    }

    public void DeleteThread(int threadId)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        var thread = Managers.PostManager.GetPost(threadId);
        foreach (var replayPost in thread.Post.Replies.ToList())
        {
          Managers.PostManager.DeleteReplyPost(replayPost.Id);
        }

        Managers.PostManager.DeletePost(threadId);
        uow.SaveChanges();
      }
    }

    public void EditPost(int postId, string content)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        var post = Managers.PostManager.GetPost(postId).Post;
        post.Content = content;
        uow.SaveChanges();
      }
    }

    public void DeleteReplyPost(int postId)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        Managers.PostManager.DeleteReplyPost(postId);
        uow.SaveChanges();
      }
    }

    public ReplyPost PublishReply(int postId, string title, string content, int publisherId)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        var post = Managers.PostManager.GetPost(postId);
        var member = Managers.MemberManager.GetMember(publisherId);
        IPostModel replyPost =
          new PostModel(new ReplyPost
        {
          Title = title,
          Content = content,
          Publisher = member.Member,
          ParentPost = post.Post
        });
        Managers.PostManager.AddReplyPost(replyPost, post);
        uow.SaveChanges();
        return (ReplyPost)replyPost.Post;
      }
    }

    public IEnumerable<Post> SearchPostsByContent(string content)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        return Managers.PostManager.GetPostByContent(content);
      }
    }
  }
}