﻿using App.Core.Interfaces.Services;
using App.DomainLayer.Helpers;

namespace App.ServiceLayer
{
  public class ReportService : BaseService, IReportService
  {
    public ReportService(ManagersProvider managers) : base(managers)
    {
    }

    public void GenerateSuperAdminReport()
    {
    }

    public void GenerateAdministratorReport()
    {
    }
  }
}