﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using App.Core;
using App.Core.Entities;
using App.Core.Interfaces.Models;
using App.Core.Interfaces.Services;
using App.DomainLayer.Helpers;

namespace App.ServiceLayer
{
  public class MemberService : BaseService, IMemberService
  {
    public MemberService(ManagersProvider managers) : base(managers)
    {
    }

    public string Login(int forumId, string username, string password)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        var superAdmins = Managers.RoleManager.GetSuperAdmins();
        var member = Managers.ForumManager.GetForum(forumId).GetMember(username);
        if (member == null)
        {
          var memberModels = superAdmins as IList<IMemberModel> ?? superAdmins.ToList();
          if (memberModels.All(m => m.Member.Name != username))
            return string.Empty;
          member = memberModels.SingleOrDefault(m => m.Member.Name == username);
          if (member == null)
            return string.Empty;
        }
        if (member.Member.IsLoggedIn)
          return string.Empty;
        if (!password.VerifyPassword(member.Member.Password))
          return string.Empty;
        var token = member.Login();
        uow.SaveChanges();
        return token;
      }
    }

    public void Logout(string token)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        Managers.MemberManager.Logout(token);
        uow.SaveChanges();
      }
    }

    public void SetMemberType(int forumId, int memberId, int typeId)
    {
    }

    public void PromoteToModerator(int subforumId, int memberId)
    {
    }

    public void DemoteModerator(int subforumId, int moderatorId)
    {
    }

    public void PromoteToAdministrator(int forumId, int memberId)
    {
    }

    public void DemoteAdministrator(int forumId, int adminId)
    {
    }

    public Member GetMember(int id)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.MemberManager.GetMember(id).Member;
      }
    }

    public Member GetMember(string name, int forumId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.MemberManager.GetMember(name, forumId).Member;
      }
    }

    public void DeleteMember(int id)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        Managers.MemberManager.DeleteMember(id);
        uow.SaveChanges();
      }
    }

    public void Confirm(string token)
    {
      var bytes = Convert.FromBase64String(token);
      var s = Encoding.UTF8.GetString(bytes);
      var name = s.Split(':')[0];
      var email = s.Split(':')[1];
      using (var uow = Managers.NewUnitOfWork())
      {
        Managers.MemberManager.Confirm(name, email);
        uow.SaveChanges();
      }
    }

    public void AddFriend(int memberId, int friendId)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        Managers.MemberManager.AddFriend(memberId, friendId);
        uow.SaveChanges();
      }
    }

    public ICollection<Member> GetFriends(int memberId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.MemberManager.GetFriends(memberId);
      }
    }

      public void DeleteFriend(int memberId, int friendId)
      {
        using (var uow = Managers.NewUnitOfWork())
        {
          Managers.MemberManager.DeleteFriend(memberId, friendId);
          uow.SaveChanges();
        }

      }

      public void BanMember(int memberId)
      {
        using (var uow = Managers.NewUnitOfWork())
        {
          Managers.MemberManager.BanMember(memberId);
          uow.SaveChanges();
        }
      }

    public bool Exists(string email)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.MemberManager.Exists(email);
      }
    }

    public bool Matches(string requestEmail, int requestedMemberId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.MemberManager.Matches(requestEmail, requestedMemberId);
      }
    }
  }
}