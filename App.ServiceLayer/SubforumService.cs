﻿using System.Collections.Generic;
using System.Linq;
using App.Core.Entities;
using App.Core.Interfaces.Services;
using App.DomainLayer.Helpers;
using App.DomainLayer.Models;

namespace App.ServiceLayer
{
  public class SubforumService : BaseService, ISubforumService
  {
    public SubforumService(ManagersProvider managers) : base(managers)
    {
    }

    public IEnumerable<Subforum> GetSubforums(int forumId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.SubforumManager.GetByForumId(forumId);
      }
    }

    public Subforum GetSubforum(int subforumId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.SubforumManager.GetSubforum(subforumId).Subforum;
      }
    }

    public Subforum GetSubforum(string subforumName)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.SubforumManager.GetSubforum(subforumName).Subforum;
      }
    }

    public Subforum CreateSubforum(int forumId, string subject)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        var subforum = new SubforumModel(new Subforum { Subject = subject });
        var parent = Managers.ForumManager.GetForum(forumId);
        Managers.SubforumManager.AddSubforum(subforum, parent);
        uow.SaveChanges();
        return subforum.Subforum;
      }
    }

    public void DeleteSubforum(int subforumId)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        Managers.SubforumManager.DeleteSubforum(subforumId);
        uow.SaveChanges();
      }
    }

    public void DeleteSubforum(string subject)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        Managers.SubforumManager.DeleteSubforum(subject);
        uow.SaveChanges();
      }
    }

    public IEnumerable<Subforum> GetModeratorSubforums(int moderatorId)
    {
      using (Managers.NewUnitOfWork())
      {
        return Managers.RoleManager.GetModeratorSubforums(moderatorId).Select(s => s.Subforum);
      }
    }

    public void PromoteToModerator(int subforumId, int memberId)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        var member = Managers.MemberManager.GetMember(memberId);
        var subforum = Managers.SubforumManager.GetSubforum(subforumId);
        Managers.RoleManager.PromoteMemberToModerator(member, subforum);
        uow.SaveChanges();
      }
    }

    public void DemoteModerator(int subforumId, int moderatorId)
    {
      using (var uow = Managers.NewUnitOfWork())
      {
        var member = Managers.MemberManager.GetMember(moderatorId);
        var subforum = Managers.SubforumManager.GetSubforum(subforumId);
        Managers.RoleManager.RevokeModeratorRole(member, subforum);
        uow.SaveChanges();
      }
    }
  }
}