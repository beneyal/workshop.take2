﻿using App.Core.Interfaces;
using App.DomainLayer.Helpers;

namespace App.ServiceLayer
{
  public abstract class BaseService
  {
    protected BaseService(ManagersProvider managers)
    {
      Managers = managers;
    }

    public IContextProvider ContextProvider { get; set; }
    public ManagersProvider Managers { get; private set; }
  }
}